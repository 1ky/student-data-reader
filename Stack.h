#ifndef Stack_h
#define Stack_h

#include "Node.h"

using namespace std;

class Stack {
private:
	Node* frontPtr;

public:
	Stack(void);
	void push(long, string, string, string, float);
	string pop(void);
	Node* get(long);
	string showNodes(void);

	bool isEmpty(void);
	void purgeStack(void);

	void removeTarget(long id);
};

Stack::Stack(void) {
	frontPtr = new Node();
}

void Stack::push(long id, string n, string sn, string m, float g) {
	Node* newPtr = new Node(id, n, sn, m, g);
	newPtr->setNext(frontPtr->getNext());
	frontPtr->setNext(newPtr);
}

string Stack::pop(void) {
	string nodeData = "";

	Node* iPtr = frontPtr->getNext();
	nodeData.append(to_string(iPtr->getIdNo()) + "\t" + iPtr->getName() + "\t" + iPtr->getSurname() + "\t" + iPtr->getMajor() + 
					"\t" + to_string(iPtr->getGpa()) + "\n");

	frontPtr->setNext(iPtr->getNext());
	delete iPtr;

	return nodeData;
}

Node* Stack::get(long id) {
	Node* iPtr = frontPtr;
	while (iPtr != nullptr) {
		if (iPtr->getIdNo() == id) {
			return iPtr;
		}
		iPtr = iPtr->getNext();
	}
	return nullptr;
}

string Stack::showNodes(void) {
	Node* iPtr = frontPtr->getNext();
	string nodeData = "";

	while (iPtr != nullptr) {
		nodeData.append(to_string(iPtr->getIdNo()) + "\t" + iPtr->getName() + "\t" + iPtr->getSurname() + "\t" + iPtr->getMajor() + 
					"\t" + to_string(iPtr->getGpa()) + "\n");

		iPtr = iPtr->getNext();
	}

	return nodeData;
}

bool Stack::isEmpty(void) {
	if (frontPtr->getNext() == nullptr)
		return true;

	return false;
}

void Stack::purgeStack(void) {
	frontPtr = new Node();
}

void Stack::removeTarget(long id) {
	Node* iPtr = frontPtr;

	while (iPtr != nullptr && iPtr->getNext() != nullptr) {
		if (iPtr->getNext()->getIdNo() == id) {
			Node* tempPtr = iPtr->getNext();
			iPtr->setNext(tempPtr->getNext());
			delete tempPtr;
		}

		iPtr = iPtr->getNext();
	}
}

#endif
