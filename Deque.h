#ifndef DEQUE_H
#define DEQUE_H

#include <iostream>
#include "Node.h"

using namespace std;

class Deque {
	private:
		Node* frontPtr;
	public:
		Deque(void);
		void enqueueHead(long, string, string, string, float);
		void enqueueTail(long, string, string, string, float);
		string dequeueHead(void);
		string dequeueTail(void);
		bool find(long);
		string showNodes(void);
		bool isEmpty(void);
		int number(void);

		string showHead(void);
		string showTail(void);
		void purgeDeque(void);

		void removeTarget(long id);
};

Deque::Deque(void) {
	frontPtr = new Node();
}

void Deque::enqueueHead(long l, string a, string b, string c, float f) {
	Node* newPtr = new Node(l, a, b, c, f);
	newPtr->setNext(frontPtr->getNext());
	frontPtr->setNext(newPtr);
}

void Deque::enqueueTail(long l, string a, string b, string c, float f) {
	Node* iPtr = frontPtr;
	while (iPtr->getNext() != nullptr) {
		iPtr = iPtr->getNext();
	}

	Node* newPtr = new Node(l, a, b, c, f);
	newPtr->setNext(iPtr->getNext());
	iPtr->setNext(newPtr);
}

string Deque::dequeueHead(void) {
	string nodeData = "";

	Node* iPtr = frontPtr->getNext();
	nodeData.append(to_string(iPtr->getIdNo()) + "\t" + iPtr->getName() + "\t" + iPtr->getSurname() + "\t" + iPtr->getMajor() + 
					"\t" + to_string(iPtr->getGpa()) + "\n");

	frontPtr->setNext(iPtr->getNext());
	delete iPtr;

	return nodeData;
}

string Deque::dequeueTail(void) {
	string nodeData = "";

	Node* iPtr = frontPtr;
	while (iPtr->getNext()->getNext() != nullptr) {
		iPtr = iPtr->getNext();
	}

	Node* tempPtr = iPtr->getNext();
	iPtr->setNext(tempPtr->getNext());
	nodeData.append(to_string(tempPtr->getIdNo()) + "\t" + tempPtr->getName() + "\t" + tempPtr->getSurname() + "\t" + tempPtr->getMajor() + 
					"\t" + to_string(tempPtr->getGpa()) + "\n");
	delete tempPtr;

	return nodeData;
}

bool Deque::find(long l) {
	Node* iPtr = frontPtr;
	while (iPtr != nullptr) {
		if (iPtr->getIdNo() == l)
			return true;

		iPtr = iPtr->getNext();
	}
	return false;
}

string Deque::showNodes(void) {
	Node* iPtr = frontPtr->getNext();
	string nodeData = "";

	while (iPtr != nullptr) {
		nodeData.append(to_string(iPtr->getIdNo()) + "\t" + iPtr->getName() + "\t" + iPtr->getSurname() + "\t" + iPtr->getMajor() + 
					"\t" + to_string(iPtr->getGpa()) + "\n");

		iPtr = iPtr->getNext();
	}

	return nodeData;
}

bool Deque::isEmpty(void) {
	if (frontPtr->getNext() == nullptr)
		return true;

	return false;
}

int Deque::number(void) {
	int i = 0;

	Node* iPtr = frontPtr;
	while (iPtr != nullptr) {
		i++;
		iPtr = iPtr->getNext();
	}

	return i;
}

string Deque::showHead(void) {
	string nodeData = "";

	if (!isEmpty()) {
		Node* iPtr = frontPtr->getNext();

		nodeData.append(to_string(iPtr->getIdNo()) + "\t" + iPtr->getName() + "\t" + iPtr->getSurname() + "\t" + iPtr->getMajor() + 
					"\t" + to_string(iPtr->getGpa()) + "\n");
	}

	return nodeData;
}

string Deque::showTail(void) {
	string nodeData = "";

	if (!isEmpty()) {
		Node* iPtr = frontPtr->getNext();
		while (iPtr->getNext() != nullptr) {
			iPtr = iPtr->getNext();
		}

		nodeData.append(to_string(iPtr->getIdNo()) + "\t" + iPtr->getName() + "\t" + iPtr->getSurname() + "\t" + iPtr->getMajor() + 
					"\t" + to_string(iPtr->getGpa()) + "\n");
	}

	return nodeData;
}

void Deque::purgeDeque(void) {
	frontPtr = new Node();
}

void Deque::removeTarget(long id) {
	Node* iPtr = frontPtr;

	while (iPtr != nullptr && iPtr->getNext() != nullptr) {
		if (iPtr->getNext()->getIdNo() == id) {
			Node* tempPtr = iPtr->getNext();
			iPtr->setNext(tempPtr->getNext());
			delete tempPtr;
		}

		iPtr = iPtr->getNext();
	}
}

#endif
