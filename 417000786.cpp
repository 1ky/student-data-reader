/************************************************************************************
  Step 1: Always include the header file wx.h
*************************************************************************************/
#include <wx/wxprec.h>
#include <wx/textdlg.h>
 
#ifndef WX_PRECOMP
  #  include <wx/wx.h>
  # include <wx/spinctrl.h>
  # include <wx/textctrl.h>   // Automatically included
  # include <wx/combobox.h>   // in versions since 2.8 
  # include <wx/listbox.h>    //     of wx.h
#endif

#include <fstream>
#include <string>
#include <bits/stdc++.h>

#include "Queue.h"
#include "Deque.h"
#include "PriorityQueue.h"
#include "Stack.h"
#include "BST.h"
#include "AVL.h"
#include "Heap.h"
#include "RB.h"
#include "SplayTree.h"
#include "BTree.h"
#include "Set.h"

#include "DataDialog.h"
#include "AboutDialog.h"


//Global pointers for the ADT containers
Queue queueList;
Deque dequeList;
PriorityQueue priorityQueueList;
Stack stackList;
BST bstList;
AVL avlList;
Heap heapList;
RB rbList;
SplayTree splayList;
BTree btreeList(2);
Set setAList;
Set setBList;

/************************************************************************************
*************************************************************************************
  Step 2: Name an inherited application class from wxApp and declare it with 
      the function to execute the program                    
*************************************************************************************
*************************************************************************************/
class MyApp: public wxApp
  {
     virtual bool OnInit();
  };

  
  
  
  
/************************************************************************************
*************************************************************************************
   Step 3: Declare the inherited main frame class from wxFrame. In this class 
       also will ALL the events handlers be declared    
*************************************************************************************
*************************************************************************************/
class ProjectFrame: public wxFrame
  {
     private:
        DECLARE_EVENT_TABLE() //To declare events items
    
     public:
        ProjectFrame(const wxString& title, const wxPoint& pos, const wxSize& size);
        
        //Functions for File Menu Items
        void OnOpenFile(wxCommandEvent& event);
        void OnSaveFile(wxCommandEvent& event);
        void OnSaveAs(wxCommandEvent& event);
        void OnDisplayFile(wxCommandEvent& event);
        void OnExit(wxCommandEvent& event);
        void OnAbout(wxCommandEvent& event);
        

        //Functions for Queue Menu Items
        void OnCreateQueue(wxCommandEvent& event);
        void OnAddQueue(wxCommandEvent& event);
        void OnDisplayQueue(wxCommandEvent& event);
        void OnShowHeadQueue(wxCommandEvent& event);
        void OnShowTailQueue(wxCommandEvent& event);
        void OnDequeueQueue(wxCommandEvent& event);


        //Functions for Deque Menu Items
        void OnCreateDeque(wxCommandEvent& event);
        void OnAddHeadDeque(wxCommandEvent& event);
        void OnAddTailDeque(wxCommandEvent& event);
        void OnDisplayDeque(wxCommandEvent& event);
        void OnShowHeadDeque(wxCommandEvent& event);
        void OnShowTailDeque(wxCommandEvent& event);
        void OnDequeueHeadDeque(wxCommandEvent& event);
        void OnDequeueTailDeque(wxCommandEvent& event);


        //Functions for Priority Queue Menu Items
        void OnCreatePriorityQueue(wxCommandEvent& event);
        void OnAddPriorityQueue(wxCommandEvent& event);
        void OnDisplayPriorityQueue(wxCommandEvent& event);
        void OnShowHeadPriorityQueue(wxCommandEvent& event);
        void OnShowTailPriorityQueue(wxCommandEvent& event);
        void OnDequeuePriorityQueue(wxCommandEvent& event);


        //Functions for Stack Menu Items
        void OnCreateStack(wxCommandEvent& event);
        void OnPopStack(wxCommandEvent& event);
        void OnPushStack(wxCommandEvent& event);
        void OnDisplayStack(wxCommandEvent& event);


        //Functions for BST Menu Items
        void OnCreateBST(wxCommandEvent& event);
        void OnAddBST(wxCommandEvent& event);
        void OnDeleteBST(wxCommandEvent& event);
        void OnInorderBST(wxCommandEvent& event);
        void OnPreorderBST(wxCommandEvent& event);
        void OnPostorderBST(wxCommandEvent& event);


        //Functions for BST Menu Items
        void OnCreateAVL(wxCommandEvent& event);
        void OnAddAVL(wxCommandEvent& event);
        void OnDeleteAVL(wxCommandEvent& event);
        void OnInorderAVL(wxCommandEvent& event);
        void OnPreorderAVL(wxCommandEvent& event);
        void OnPostorderAVL(wxCommandEvent& event);

        //Functions for Heap Menu Items
        void OnCreateHeap(wxCommandEvent& event);
        void OnAddHeap(wxCommandEvent& event);
        void OnDeleteHeap(wxCommandEvent& event);
        void OnDisplayHeap(wxCommandEvent& event);

        //Functions for RB Menu Items
        void OnCreateRB(wxCommandEvent& event);
        void OnAddRB(wxCommandEvent& event);
        void OnDeleteRB(wxCommandEvent& event);
        void OnInorderRB(wxCommandEvent& event);
        void OnPreorderRB(wxCommandEvent& event);
        void OnPostorderRB(wxCommandEvent& event);

        //Functions for Splay Menu Items
        void OnCreateSplay(wxCommandEvent& event);
        void OnAddSplay(wxCommandEvent& event);
        void OnDeleteSplay(wxCommandEvent& event);
        void OnInorderSplay(wxCommandEvent& event);
        void OnPreorderSplay(wxCommandEvent& event);
        void OnPostorderSplay(wxCommandEvent& event);

        //Functions for BTree Menu Items
        void OnCreateBTree(wxCommandEvent& event);
        void OnAddBTree(wxCommandEvent& event);
        void OnFindDataBTree(wxCommandEvent& event);
        void OnDisplayBTree(wxCommandEvent& event);

        //Functions for Set Menu Items
        void OnCreateSets(wxCommandEvent& event);
        void OnAddSet(wxCommandEvent& event);
        void OnDisplaySetA(wxCommandEvent& event);
        void OnDisplaySetB(wxCommandEvent& event);
        void OnDisplayIntersection(wxCommandEvent& event);
        void OnDisplayUnion(wxCommandEvent& event);
        void OnDeleteSetA(wxCommandEvent& event);
        void OnDeleteSetB(wxCommandEvent& event);
        
        
        //Public attributes
        wxTextCtrl* MainEditBox;
        wxTextCtrl* filenameTextBox;
        wxString CurrentDocPath;    // The Path to the file we have open

  };

 
  
/************************************************************************************
*************************************************************************************
  Step 4. Declare the compiler directives                    
*************************************************************************************
*************************************************************************************/
DECLARE_APP(MyApp)        // Declare Application class
IMPLEMENT_APP(MyApp)      // Create Application class object
  


enum
    {
          // File Sub-menu items
         ID_Exit = wxID_HIGHEST + 1,
         ID_OpenFile,
         ID_SaveFile,
         ID_SaveAs,
         ID_DisplayFile,
         ID_About,

         // Display File Sub-menu items
         menuDisplayFile,

         // Queue Sub-menu items
         Queue_CreateQueue,
         Queue_AddData,
         Queue_DisplayAll,
         Queue_ShowHead,
         Queue_ShowTail,
         Queue_Dequeue,

         // Deque Sub-menu items
         Deque_CreateDeque,
         Deque_AddHead,
         Deque_AddTail,
         Deque_DisplayAll,
         Deque_ShowHead,
         Deque_ShowTail,
         Deque_DequeueHead,
         Deque_DequeueTail,

         // Priority Queue Sub-menu items
         PriorityQueue_CreateQueue,
         PriorityQueue_AddData,
         PriorityQueue_DisplayAll,
         PriorityQueue_ShowHead,
         PriorityQueue_ShowTail,
         PriorityQueue_Dequeue,

         // Stack Sub-menu items
         Stack_CreateStack,
         Stack_Pop,
         Stack_Push,
         Stack_DisplayAll,

         // BST Sub-menu items
         BST_CreateBST,
         BST_AddData,
         BST_DeleteData,
         BST_Inorder,
         BST_Preorder,
         BST_Postorder,

         // AVL Tree Sub-menu items
         AVL_CreateAVL,
         AVL_AddData,
         AVL_DeleteData,
         AVL_Inorder,
         AVL_Preorder,
         AVL_Postorder,

         // Heap Sub-menu items
         Heap_CreateHeap,
         Heap_AddData,
         Heap_DeleteData,
         Heap_DisplayAll,
         Heap_HeapSort,

         // RB Tree Sub-menu items
         RB_CreateRBT,
         RB_AddData,
         RB_DeleteData,
         RB_Inorder,
         RB_Preorder,
         RB_Postorder,

         // Splay Sub-menu items
         Splay_CreateSplay,
         Splay_AddData,
         Splay_DeleteData,
         Splay_Inorder,
         Splay_Preorder,
         Splay_Postorder,

         // B-Tree Sub-menu items
         B_CreateBTree,
         B_AddData,
         B_FindRecord,
         B_DisplayAll,

         // Set Sub-menu items
         Set_CreateSets,
         Set_AddData,
         Set_DisplaySetA,
         Set_DisplaySetB,
         Set_DisplayIntersection,
         Set_DisplayUnion,
         Set_DeleteFromSetA,
         Set_DeleteFromSetB,
    };

    
    
BEGIN_EVENT_TABLE ( ProjectFrame, wxFrame )
      // Events for the "File" menu items
      EVT_MENU ( ID_OpenFile,    ProjectFrame::OnOpenFile)
      EVT_MENU ( ID_SaveFile,    ProjectFrame::OnSaveFile)
      EVT_MENU ( ID_SaveAs,    ProjectFrame::OnSaveAs)
      EVT_MENU ( ID_DisplayFile,    ProjectFrame::OnDisplayFile)
      EVT_MENU ( ID_Exit,  ProjectFrame::OnExit )
      EVT_MENU ( ID_About, ProjectFrame::OnAbout)

      // Events for the "Display File" menu items
      EVT_MENU (menuDisplayFile, ProjectFrame::OnDisplayFile)

      // Events for the "Queue" menu items
      EVT_MENU ( Queue_CreateQueue, ProjectFrame::OnCreateQueue)
      EVT_MENU ( Queue_AddData, ProjectFrame::OnAddQueue)
      EVT_MENU ( Queue_DisplayAll, ProjectFrame::OnDisplayQueue)
      EVT_MENU ( Queue_ShowHead, ProjectFrame::OnShowHeadQueue)
      EVT_MENU ( Queue_ShowTail, ProjectFrame::OnShowTailQueue)
      EVT_MENU ( Queue_Dequeue, ProjectFrame::OnDequeueQueue)

      // Events for the "Deque" menu items
      EVT_MENU ( Deque_CreateDeque, ProjectFrame::OnCreateDeque)
      EVT_MENU ( Deque_AddHead, ProjectFrame::OnAddHeadDeque)
      EVT_MENU ( Deque_AddTail, ProjectFrame::OnAddTailDeque)
      EVT_MENU ( Deque_DisplayAll, ProjectFrame::OnDisplayDeque)
      EVT_MENU ( Deque_ShowHead, ProjectFrame::OnShowHeadDeque)
      EVT_MENU ( Deque_ShowTail, ProjectFrame::OnShowTailDeque)
      EVT_MENU ( Deque_DequeueHead, ProjectFrame::OnDequeueHeadDeque)
      EVT_MENU ( Deque_DequeueTail, ProjectFrame::OnDequeueTailDeque)

      // Events for the "Priority Queue" menu items
      EVT_MENU ( PriorityQueue_CreateQueue, ProjectFrame::OnCreatePriorityQueue)
      EVT_MENU ( PriorityQueue_AddData, ProjectFrame::OnAddPriorityQueue)
      EVT_MENU ( PriorityQueue_DisplayAll, ProjectFrame::OnDisplayPriorityQueue)
      EVT_MENU ( PriorityQueue_ShowHead, ProjectFrame::OnShowHeadPriorityQueue)
      EVT_MENU ( PriorityQueue_ShowTail, ProjectFrame::OnShowTailPriorityQueue)
      EVT_MENU ( PriorityQueue_Dequeue, ProjectFrame::OnDequeuePriorityQueue)

      // Events for the "Stack" menu items
      EVT_MENU ( Stack_CreateStack, ProjectFrame::OnCreateStack)
      EVT_MENU ( Stack_Pop, ProjectFrame::OnPopStack)
      EVT_MENU ( Stack_Push, ProjectFrame::OnPushStack)
      EVT_MENU ( Stack_DisplayAll, ProjectFrame::OnDisplayStack)

      // Events for the "BST" menu items
      EVT_MENU ( BST_CreateBST, ProjectFrame::OnCreateBST)
      EVT_MENU ( BST_AddData, ProjectFrame::OnAddBST)
      EVT_MENU ( BST_DeleteData, ProjectFrame::OnDeleteBST)
      EVT_MENU ( BST_Inorder, ProjectFrame::OnInorderBST)
      EVT_MENU ( BST_Preorder, ProjectFrame::OnPreorderBST)
      EVT_MENU ( BST_Postorder, ProjectFrame::OnPostorderBST)

      // Events for the "AVL" menu items
      EVT_MENU ( AVL_CreateAVL, ProjectFrame::OnCreateAVL)
      EVT_MENU ( AVL_AddData, ProjectFrame::OnAddAVL)
      EVT_MENU ( AVL_DeleteData, ProjectFrame::OnDeleteAVL)
      EVT_MENU ( AVL_Inorder, ProjectFrame::OnInorderAVL)
      EVT_MENU ( AVL_Preorder, ProjectFrame::OnPreorderAVL)
      EVT_MENU ( AVL_Postorder, ProjectFrame::OnPostorderAVL)

      // Events for the "Heap" menu items
      EVT_MENU ( Heap_CreateHeap, ProjectFrame::OnCreateHeap)
      EVT_MENU ( Heap_AddData, ProjectFrame::OnAddHeap)
      EVT_MENU ( Heap_DeleteData, ProjectFrame::OnDeleteHeap)
      EVT_MENU ( Heap_DisplayAll, ProjectFrame::OnDisplayHeap)

      // Events for the "RB" menu items
      EVT_MENU ( RB_CreateRBT, ProjectFrame::OnCreateRB)
      EVT_MENU ( RB_AddData, ProjectFrame::OnAddRB)
      EVT_MENU ( RB_DeleteData, ProjectFrame::OnDeleteRB)
      EVT_MENU ( RB_Inorder, ProjectFrame::OnInorderRB)
      EVT_MENU ( RB_Preorder, ProjectFrame::OnPreorderRB)
      EVT_MENU ( RB_Postorder, ProjectFrame::OnPostorderRB)

      // Events for the "Splay" menu items
      EVT_MENU (Splay_CreateSplay, ProjectFrame::OnCreateSplay)
      EVT_MENU (Splay_AddData, ProjectFrame::OnAddSplay)
      EVT_MENU (Splay_DeleteData, ProjectFrame::OnDeleteSplay)
      EVT_MENU (Splay_Inorder, ProjectFrame::OnInorderSplay)
      EVT_MENU (Splay_Preorder, ProjectFrame::OnPreorderSplay)
      EVT_MENU (Splay_Postorder, ProjectFrame::OnPostorderSplay)

      //Events for the "BTree" menu items
      EVT_MENU (B_CreateBTree, ProjectFrame::OnCreateBTree)
      EVT_MENU (B_AddData, ProjectFrame::OnAddBTree)
      EVT_MENU (B_FindRecord, ProjectFrame::OnFindDataBTree)
      EVT_MENU (B_DisplayAll, ProjectFrame::OnDisplayBTree)

      // Events for the "Set" menu items
      EVT_MENU (Set_CreateSets, ProjectFrame::OnCreateSets)
      EVT_MENU (Set_AddData, ProjectFrame::OnAddSet)
      EVT_MENU (Set_DisplaySetA, ProjectFrame::OnDisplaySetA)
      EVT_MENU (Set_DisplaySetB, ProjectFrame::OnDisplaySetB)
      EVT_MENU (Set_DisplayIntersection, ProjectFrame::OnDisplayIntersection)
      EVT_MENU (Set_DisplayUnion, ProjectFrame::OnDisplayUnion)
      EVT_MENU (Set_DeleteFromSetA, ProjectFrame::OnDeleteSetA)
      EVT_MENU (Set_DeleteFromSetB, ProjectFrame::OnDeleteSetB)

END_EVENT_TABLE () 
  




/************************************************************************************
*************************************************************************************
  Step 5.  Define the Application class function to initialize the application
*************************************************************************************
*************************************************************************************/
bool MyApp::OnInit()
  {
      // Create the main application window
      ProjectFrame *frame = new ProjectFrame( wxT("COMP2611 - Data Structures Project"), 
                    wxPoint(50,50), wxSize(800,600) );

      // Display the window
      frame->Show(TRUE);
    
      SetTopWindow(frame);

      return TRUE;

  }
  
  
  

/************************************************************************************
*************************************************************************************
  Step 6:   Define the Constructor functions for the Frame class
*************************************************************************************
*************************************************************************************/
ProjectFrame::ProjectFrame ( const wxString& title, const wxPoint& pos, const wxSize& size)
            : wxFrame((wxFrame *)NULL, -1, title, pos, size)
  {
    // Set the frame icon - optional
    // SetIcon(wxIcon(wxT("uwiIcon.xpm")));

    
    
    // Create the main-menu items
    wxMenu *menuFile = new wxMenu;
    wxMenu *menuDisplayFile = new wxMenu;
    wxMenu *menuQueue = new wxMenu;
    wxMenu *menuDeque = new wxMenu;
    wxMenu *menuPriorityQueue = new wxMenu;
    wxMenu *menuStack = new wxMenu;
    wxMenu *menuBST = new wxMenu;
    wxMenu *menuAVLTree = new wxMenu;
    wxMenu *menuHeap = new wxMenu;
    wxMenu *menuRBTree = new wxMenu;
    wxMenu *menuSplay = new wxMenu;
    wxMenu *menuB_Tree = new wxMenu;
    wxMenu *menuSet = new wxMenu;
    wxMenu *menuHelp = new wxMenu;

    
    
    //Create a Main menu bar
    wxMenuBar *menuBar = new wxMenuBar;
        
    
    //Append the main menu items to the Menu Bar
    menuBar->Append(menuFile, wxT("&File"));
    menuBar->Append(menuDisplayFile, wxT("&Display File"));
    menuBar->Append(menuQueue, wxT("&Queue"));
    menuBar->Append(menuDeque, wxT("&Deque"));
    menuBar->Append(menuPriorityQueue, wxT("&Priority Queue"));
    menuBar->Append(menuStack, wxT("&Stack"));
    menuBar->Append(menuBST, wxT("&BST"));
    menuBar->Append(menuAVLTree, wxT("&AVL Tree"));
    menuBar->Append(menuHeap, wxT("&Heap"));
    menuBar->Append(menuRBTree, wxT("&RB Tree"));
    menuBar->Append(menuSplay, wxT("&Splay"));
    menuBar->Append(menuB_Tree, wxT("&B-Tree"));
    menuBar->Append(menuSet, wxT("&Set"));
    menuBar->Append( menuHelp,   wxT("&Help") );
    
    
    //Append the sub-menu items to the menuFile Main Menu item
    menuFile->Append(ID_OpenFile, wxT("O&pen File"), wxT("Read Data from a File"));
    menuFile->Append(ID_SaveFile, wxT("S&ave File"), wxT("Save Data"));
    menuFile->Append(ID_SaveAs, wxT("Save As..."), wxT("Save Data As..."));
    menuFile->Append(ID_DisplayFile, wxT("Display File"), wxT("Show Contents of Selected File"));
    menuFile->Append( ID_Exit, wxT("E&xit"),  wxT("Close and EXIT Program") );


    //Append the sub-menu items to the menuQueue Main Menu item
    menuQueue->Append(Queue_CreateQueue, wxT("Create Queue"), wxT("Create a new Queue"));
    menuQueue->Append(Queue_AddData, wxT("Add Data"), wxT("Add Data to the Queue"));
    menuQueue->Append(Queue_DisplayAll, wxT("Display All"), wxT("Display All Data Currently in Queue"));
    menuQueue->Append(Queue_ShowHead, wxT("Show Head"), wxT("Display Head of the Queue"));
    menuQueue->Append(Queue_ShowTail, wxT("Show Tail"), wxT("Display Tail of the Queue"));
    menuQueue->Append(Queue_Dequeue, wxT("Dequeue"), wxT("Remove Item from the Queue"));


    //Append the sub-menu items to the menuDeque Main Menu item
    menuDeque->Append(Deque_CreateDeque, wxT("Create Deque"), wxT("Create a new Deque"));
    menuDeque->Append(Deque_AddHead, wxT("Add Head"), wxT("Add Data to the Head of the Deque"));
    menuDeque->Append(Deque_AddTail, wxT("Add Tail"), wxT("Add Data to the Tail of the Deque"));
    menuDeque->Append(Deque_DisplayAll, wxT("Display All"), wxT("Display All Data Currently in Deque"));
    menuDeque->Append(Deque_ShowHead, wxT("Show Head"), wxT("Display Head of the Deque"));
    menuDeque->Append(Deque_ShowTail, wxT("Show Tail"), wxT("Display Tail of the Deque"));
    menuDeque->Append(Deque_DequeueHead, wxT("Dequeue Head"), wxT("Dequeue Data from the Head of Deque"));
    menuDeque->Append(Deque_DequeueTail, wxT("Dequeue Tail"), wxT("Dequeue Data from the Tail of Deque"));


    //Append the sub-menu items to the menuPriorityQueue Main Menu item
    menuPriorityQueue->Append(PriorityQueue_CreateQueue, wxT("Create Priority Queue"), wxT("Create a new Priority Queue"));
    menuPriorityQueue->Append(PriorityQueue_AddData, wxT("Add Data"), wxT("Add Data to the Queue"));
    menuPriorityQueue->Append(PriorityQueue_DisplayAll, wxT("Display All"), wxT("Display All Data Currently in Priority Queue"));
    menuPriorityQueue->Append(PriorityQueue_ShowHead, wxT("Show Head"), wxT("Display Head of the Priority Queue"));
    menuPriorityQueue->Append(PriorityQueue_ShowTail, wxT("Show Tail"), wxT("Display Tail of the Priority Queue"));
    menuPriorityQueue->Append(PriorityQueue_Dequeue, wxT("Dequeue"), wxT("Remove Item from the Priority Queue"));


    //Append the sub-menu items to the menuStack Main Menu item
    menuStack->Append(Stack_CreateStack, wxT("Create Stack"), wxT("Create a new Stack"));
    menuStack->Append(Stack_Pop, wxT("Pop"), wxT("Add Data to the Stack"));
    menuStack->Append(Stack_Push, wxT("Push"), wxT("Remove Data from the Stack"));
    menuStack->Append(Stack_DisplayAll, wxT("Display All"), wxT("Display All Items in Stack"));


    //Append the sub-menu items to the menuBST Main Menu item
    menuBST->Append(BST_CreateBST, wxT("Create BST"), wxT("Create a new BST"));
    menuBST->Append(BST_AddData, wxT("Add Data"), wxT("Add Data to the BST"));
    menuBST->Append(BST_DeleteData, wxT("Delete Data"), wxT("Delete Data from BST"));
    menuBST->Append(BST_Inorder, wxT("Inorder"), wxT("Displays Data in BST In Order"));
    menuBST->Append(BST_Preorder, wxT("Preorder"), wxT("Displays Data in BST Pre Order"));
    menuBST->Append(BST_Postorder, wxT("Postorder"), wxT("Displays Data in BST Post Order"));


    //Append the sub-menu items to the menuAVLTree Main Menu item
    menuAVLTree->Append(AVL_CreateAVL, wxT("Create AVL"), wxT("Create a new AVL Tree"));
    menuAVLTree->Append(AVL_AddData, wxT("Add Data"), wxT("Add Data to the AVL Tree"));
    menuAVLTree->Append(AVL_DeleteData, wxT("Delete Data"), wxT("Delete Data from AVL Tree"));
    menuAVLTree->Append(AVL_Inorder, wxT("Inorder"), wxT("Displays Data in AVL Tree In Order"));
    menuAVLTree->Append(AVL_Preorder, wxT("Preorder"), wxT("Displays Data in AVL Tree Pre Order"));
    menuAVLTree->Append(AVL_Postorder, wxT("Postorder"), wxT("Displays Data in AVL Tree Post Order"));

    //Append the sub-menu items to the menuHeap Main Menu item
    menuHeap->Append(Heap_CreateHeap, wxT("Create Heap"), wxT("Create a new Heap"));
    menuHeap->Append(Heap_AddData, wxT("Add Data"), wxT("Add Data to the Heap"));
    menuHeap->Append(Heap_DeleteData, wxT("Delete Data"), wxT("Delete Data from Heap"));
    menuHeap->Append(Heap_DisplayAll, wxT("Display All"), wxT("Displays Data in Heap"));
    menuHeap->Append(Heap_HeapSort, wxT("Heap Sort"), wxT("Sorts Heap"));

    //Append the sub-menu items to the menuRBTree Main Menu item
    menuRBTree->Append(RB_CreateRBT, wxT("Create RBT"), wxT("Create a new RB Tree"));
    menuRBTree->Append(RB_AddData, wxT("Add Data"), wxT("Add Data to the RB Tree"));
    menuRBTree->Append(RB_DeleteData, wxT("Delete Data"), wxT("Delete Data from RB Tree"));
    menuRBTree->Append(RB_Inorder, wxT("Inorder"), wxT("Displays Data in RB Tree In Order"));
    menuRBTree->Append(RB_Preorder, wxT("Preorder"), wxT("Displays Data in RB Tree Pre Order"));
    menuRBTree->Append(RB_Postorder, wxT("Postorder"), wxT("Displays Data in RB Tree Post Order"));

    //Append the sub-menu items to the menuSplay Main Menu item
    menuSplay->Append(Splay_CreateSplay, wxT("Create Splay"), wxT("Create a new Splay Tree"));
    menuSplay->Append(Splay_AddData, wxT("Add Data"), wxT("Add Data to the Splay Tree"));
    menuSplay->Append(Splay_DeleteData, wxT("Delete Data"), wxT("Delete Data from Splay Tree"));
    menuSplay->Append(Splay_Inorder, wxT("Inorder"), wxT("Displays Data in Splay Tree In Order"));
    menuSplay->Append(Splay_Preorder, wxT("Preorder"), wxT("Displays Data in Splay Tree Pre Order"));
    menuSplay->Append(Splay_Postorder, wxT("Postorder"), wxT("Displays Data in Splay Tree Post Order"));

    //Append the sub-menu items to the menuB-Tree Main Menu item
    menuB_Tree->Append(B_CreateBTree, wxT("Create B-Tree"), wxT("Create a new BTree"));
    menuB_Tree->Append(B_AddData, wxT("Add Data"), wxT("Add Data to the BTree"));
    menuB_Tree->Append(B_FindRecord, wxT("Find Record"), wxT("Find Data Stored in BTree"));
    menuB_Tree->Append(B_DisplayAll, wxT("Display All"), wxT("Displays Data in BTree"));

    //Append the sub-menu items to the menuSET Main Menu item
    menuSet->Append(Set_CreateSets, wxT("Create Sets"), wxT("Create Two Sets (A and B)"));
    menuSet->Append(Set_AddData, wxT("Add Data"), wxT("Add Data to a Set"));
    menuSet->Append(Set_DisplaySetA, wxT("Display SetA"), wxT("Display Data in Set A"));
    menuSet->Append(Set_DisplaySetB, wxT("Display SetB"), wxT("Display Data in Set B"));
    menuSet->Append(Set_DisplayIntersection, wxT("Display Intersection"), wxT("Display Intersection of Set A and Set B"));
    menuSet->Append(Set_DisplayUnion, wxT("Display Union"), wxT("Display Union of Set A and Set B"));
    menuSet->Append(Set_DeleteFromSetA, wxT("Delete From SetA"), wxT("Delete Data from Set A"));
    menuSet->Append(Set_DeleteFromSetB, wxT("Delete From SetB"), wxT("Delete Data from Set B"));

    //Append the sub-menu items to the menuHelp Main Menu item
    menuHelp->Append( ID_About, wxT("About"), wxT("Displays Useful Information About Project"));
    menuHelp->Append( ID_Exit, wxT("E&xit"),  wxT("Close and EXIT Program") );
    
    
    // ... and now... attach this main menu bar to the frame
    SetMenuBar( menuBar );
       
    
    // Create a status bar just for fun
    CreateStatusBar(3);
      
    
    //Put something in the first section of the status bar
    SetStatusText( wxT("Ready...") );

    //Put something in the Second section of the status bar
    SetStatusText( wxT("   Assignment: Kyle Shepherd"), 1 );
    
    //Put something in the Third section of the status bar
    SetStatusText( wxT("    417000786"), 2 );


    //Set up the panel for data display
//===============================================================================
//================= DO NOT CHANGE THE CODE IN THIS SECTION ======================    
//===============================================================================

    wxPanel     *panel     = new wxPanel(this, -1);
    wxBoxSizer  *vbox      = new wxBoxSizer(wxVERTICAL);        //Vertical sizer for main window
    wxBoxSizer  *hbox1     = new wxBoxSizer(wxHORIZONTAL);        //Horizontal sizer for main window

    //Add two textboxes to the panel for data display
    wxBoxSizer  *hbox2     = new wxBoxSizer(wxHORIZONTAL);        //Horizontal sizer for filename window
    wxBoxSizer  *hbox3     = new wxBoxSizer(wxHORIZONTAL);        //Horizontal sizer for display window
    
    wxStaticText *fileLabel     = new wxStaticText(panel, wxID_ANY, wxT("File Name"));
    wxStaticText *displayLabel     = new wxStaticText(panel, wxID_ANY, wxT("Display"));

    //Initialize the filename textbox window
    filenameTextBox = new wxTextCtrl(panel, wxID_ANY, wxT("No File Opened Yet..."));
    
    //Initialize the display window
    MainEditBox = new wxTextCtrl(panel, wxID_ANY, wxT("No Data Available Yet..."), 
                      wxPoint(-1, -1), wxSize(-1, -1), wxTE_MULTILINE | wxTE_RICH);
                      

    //Position the labels and textboxes in the panel
    hbox1->Add(fileLabel, 0, wxRIGHT, 8);
    hbox1->Add(filenameTextBox, 1);
    vbox->Add(hbox1, 0, wxEXPAND | wxLEFT | wxRIGHT | wxTOP, 10);

    vbox->Add(-1, 10);
    hbox2->Add(displayLabel, 0);
    vbox->Add(hbox2, 0, wxLEFT | wxTOP, 10);
    vbox->Add(-1, 10);

    hbox3->Add(MainEditBox, 1, wxEXPAND);
    vbox->Add(hbox3, 1, wxLEFT | wxRIGHT | wxEXPAND, 10);

    vbox->Add(-1, 25);
    panel->SetSizer(vbox);

    Centre();
    
  }



//============+++======================================================
//=========== Other Support Functions =================================
//=====================================================================
    
  
string makeRecord(long num, string first, string last, string maj, float score)
    {
        string record;
        
        record = to_string(num);
        record.append(" \t\t");
        record.append(first);
        record.append(" \t");
        record.append(last);
        record.append(" \t");
        record.append("  \t\t");
        record.append(maj);
        record.append(" \t");
        record.append(to_string(score));
        
        return record;
    }
  
  
/**************************************************************************
***************************************************************************
  Step 7:  Define member functions for the Frame class
***************************************************************************
***************************************************************************/

//=========================================================================
//======== Definition for the File Functions ==============================
//=========================================================================

  void ProjectFrame::OnOpenFile(wxCommandEvent& event )
    {
           // Creates a "open file" dialog with 3 file types
        wxFileDialog *OpenDialog = new wxFileDialog( this, 
            (wxT("Choose a file to open")), wxEmptyString, wxEmptyString,
             (wxT("Text files (*.txt)|*.txt|Data Files (*.dat)|*.dat|All files (*.*)|*.*")), wxFD_OPEN, wxDefaultPosition);
 
        if (OpenDialog->ShowModal() == wxID_OK)    // if the user click "Open" instead of "cancel"
        {
            // Sets our current document to the file the user selected
            CurrentDocPath = OpenDialog->GetPath();

            //Clean up filename textbox and Display file name in filename textbox 
            filenameTextBox->Clear();
            filenameTextBox->AppendText(CurrentDocPath);

            MainEditBox->LoadFile(CurrentDocPath);   //Opens that file in the MainEditBox

            // Set the Title
            SetTitle(wxString(wxT("COMP2611 - Data Structures : 417000786")));
        }

    }

  void ProjectFrame::OnSaveFile(wxCommandEvent& event)
  {
      // Save to already set path
      MainEditBox->SaveFile(CurrentDocPath);
  }

  void ProjectFrame::OnSaveAs(wxCommandEvent& event) 
  {
      wxFileDialog *SaveAsDialog = new wxFileDialog(this, 
            (wxT("Save File As _?")), wxEmptyString, wxEmptyString,
            (wxT("Text files (*.txt)|*.txt|Data Files (*.dat)|*.dat|All files (*.*)|*.*")), wxFD_SAVE | wxFD_OVERWRITE_PROMPT,
             wxDefaultPosition);

      if (SaveAsDialog->ShowModal() == wxID_OK)    // if the user click "Save" instead of "cancel"
        {
            // Creates a Save Dialog
            CurrentDocPath = SaveAsDialog->GetPath();
            // Sets our current document to the file the user selected
            MainEditBox->SaveFile(CurrentDocPath);
            // Set the Title
            SetTitle(wxString(wxT("COMP2611 - Data Structures : 417000786")));
        }
  }

  void ProjectFrame::OnDisplayFile(wxCommandEvent& event)
  {
    if (CurrentDocPath.Len() == 0)
    {
      wxMessageBox(wxT("No File is Currently Loaded!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
    }
    else
    {
      MainEditBox->LoadFile(CurrentDocPath);
    }
  }

  
void ProjectFrame::OnExit(wxCommandEvent& event)
    {
        wxMessageBox(wxT("Good-bye!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
        Close(TRUE); // Close the window
    }

void ProjectFrame::OnAbout(wxCommandEvent& event)
{
  AboutDialog* dlg = new AboutDialog( wxT("COMP2611 - About"), 
               wxPoint(200,200), wxSize(500,300) );

  dlg->ShowModal();
  dlg->Close();
  dlg->Destroy();
}


//=============================================================================
//============== END Definition for the File Functions ========================
//=============================================================================

//=============================================================================
//============== Definition for the Queue Functions ===========================
//=============================================================================

void ProjectFrame::OnCreateQueue(wxCommandEvent& event)
{
        long    num;
        string  first;
        string  last;
        string  maj;
        float   score;
 //       string  line;

        string record, heading, dummyString;
        
    // Clear the edit box
    MainEditBox->Clear();

    //Empty the Queue
    queueList.purgeQueue();   // Write that function!
      
    //open the file
    ifstream infile(CurrentDocPath.mb_str(), ios::in);

    // Check to see if a valid file was opened.
    if (!infile)
      {
          MainEditBox->AppendText(wxT("\n\n\t\t\t\tNo Data file opened as yet...\n\n"));
        return;
      }
    
        // Read off the heading line
        infile >> dummyString >> dummyString >> dummyString >> dummyString >> dummyString;
  
        // Create a label for the displayed records
        heading = "IDNo \tName \t\t\tSurname \t\tMajor \t\tGPA\n";
        heading.append("=====================================================\n");

        
        // Convert "heading" into a wxString and display it in the MainEditBox
        wxString headingLine(heading.c_str(), wxConvUTF8);
        MainEditBox->AppendText(headingLine);

        while (!infile.eof())
            {
                infile >> num >> first >> last >> maj >> score;
 
                if (!infile.eof())
                    {
                        if (maj == "COMP") {
                            record = makeRecord(num, first, last, maj, score);
                                    
                            record.append("\t\t...Enqueued\n");
                            queueList.enqueue(num, first, last, maj, score);
                                                    
                            wxString wxRecord(record.c_str(), wxConvUTF8);
                            MainEditBox->AppendText(wxRecord);
                            
                            //Reset record variable
                            record = "";
                        }
                    }
            }  // End while
}

void ProjectFrame::OnAddQueue(wxCommandEvent& event)
{
    if (TRUE) {
        long    num;
        string  first;
        string  last;
        string  maj;
        float   score;

        string holder = "";
        string record;

        DataDialog* dlg = new DataDialog( wxT("COMP2611 - Data Entry"), 
               wxPoint(200,200), wxSize(600,300) );

        if (dlg->ShowModal() == wxID_OK) {
            num = dlg->IdNoEditBox->GetValue();
            first = dlg->NameEditBox->GetValue();
            last = dlg->SurnameEditBox->GetValue();
            maj = dlg->majorCombo->GetValue();
            score = dlg->GPAEditBox->GetValue();

            if (maj == "COMP") {
                queueList.enqueue(num, first, last, maj, score);
                priorityQueueList.enqueue(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                avlList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "IT") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                stackList.push(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                rbList.insert(num, first, last, maj, score);
                btreeList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "MATH") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                dequeList.enqueueHead(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                splayList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "ELET") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                dequeList.enqueueTail(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "PHYS") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            }

            OnDisplayQueue(event);
        } 
        else {
          dlg->Close();
        }
        dlg->Destroy();
    }
}

void ProjectFrame::OnDisplayQueue(wxCommandEvent& event)
  {
    if (!queueList.isEmpty()) {
      // Clear the edit box
      MainEditBox->Clear();     

      string records = queueList.showNodes();
        
      //Convert to a wxString
      wxString wxRecords(records.c_str(), wxConvUTF8);

      //display the words in the MainEditBox
      MainEditBox->AppendText(wxT("\t\t\t\t\nRecords in Queue:\n\n"));
      MainEditBox->AppendText(wxRecords);
    } else {
        wxMessageBox(wxT("No records are saved in this ADT!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
    }
  }

void ProjectFrame::OnShowHeadQueue(wxCommandEvent& event)
{
    string heading, records;

    if (!queueList.isEmpty())
    {
        heading = "IDNo \tName \t\t\tSurname \t\tMajor \t\tGPA\n";
        heading.append("=====================================================\n");

        records = queueList.showHead();

        // Clear the edit box
        MainEditBox->Clear();
        MainEditBox->AppendText(heading);
        MainEditBox->AppendText(records);
    } else {
        wxMessageBox(wxT("No records are saved in this ADT!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
    }
}

void ProjectFrame::OnShowTailQueue(wxCommandEvent& event)
{
    string heading, records;

    if (!queueList.isEmpty())
    {
        heading = "IDNo \tName \t\t\tSurname \t\tMajor \t\tGPA\n";
        heading.append("=====================================================\n");

        records = queueList.showTail();

        // Clear the edit box
        MainEditBox->Clear();
        MainEditBox->AppendText(heading);
        MainEditBox->AppendText(records);
    } else {
        wxMessageBox(wxT("No records are saved in this ADT!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
    }
}

void ProjectFrame::OnDequeueQueue(wxCommandEvent& event)
{
    long    num;
    string  first;
    string  last;
    string  maj;
    float   score;

    string holder = "";
    string record;
    string heading;

    if (!queueList.isEmpty())
    {
        heading = "Record Removed from Queue\n\n";
        heading.append("IDNo \tName \t\t\tSurname \t\tMajor \t\tGPA\n");
        heading.append("=====================================================\n");

        holder = queueList.dequeue();

        stringstream record(holder);
        record >> num >> first >> last >> maj >> score;

        queueList.removeTarget(num);
        dequeList.removeTarget(num);
        priorityQueueList.removeTarget(num);
        stackList.removeTarget(num);
        bstList.remove(num);
        avlList.remove(num);
        rbList.remove(num);
        //heapList.remove(num);
        splayList.remove(num);
        //btreeList.remove(num);
        setAList.delNode(num);
        setBList.delNode(num);

        // Clear the edit box
        MainEditBox->Clear();
        MainEditBox->AppendText(heading);
        MainEditBox->AppendText(holder);
    } else {
        wxMessageBox(wxT("No records are saved in this ADT!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
    }
}


//=============================================================================
//============== END Definition for the Queue Functions =======================
//=============================================================================

//=============================================================================
//============== Definition for the Deque Functions ===========================
//=============================================================================

void ProjectFrame::OnCreateDeque(wxCommandEvent& event)
{
        long    num;
        string  first;
        string  last;
        string  maj;
        float   score;
 //       string  line;

        string record, heading, dummyString;
        
    // Clear the edit box
    MainEditBox->Clear();

    //Empty the Queue
    dequeList.purgeDeque();   // Write that function!
      
    //open the file
    ifstream infile(CurrentDocPath.mb_str(), ios::in);

    // Check to see if a valid file was opened.
    if (!infile)
      {
          MainEditBox->AppendText(wxT("\n\n\t\t\t\tNo Data file opened as yet...\n\n"));
        return;
      }
    
        // Read off the heading line
        infile >> dummyString >> dummyString >> dummyString >> dummyString >> dummyString;
  
        // Create a label for the displayed records
        heading = "IDNo \tName \t\t\tSurname \t\tMajor \t\tGPA\n";
        heading.append("=====================================================\n");

        
        // Convert "heading" into a wxString and display it in the MainEditBox
        wxString headingLine(heading.c_str(), wxConvUTF8);
        MainEditBox->AppendText(headingLine);

        while (!infile.eof())
            {
                infile >> num >> first >> last >> maj >> score;
 
                if (!infile.eof())
                    {
                        if (maj == "MATH") {
                            record = makeRecord(num, first, last, maj, score);
                                    
                            record.append("\t\t...Enqueued at Head\n");
                            dequeList.enqueueHead(num, first, last, maj, score);
                                                    
                            wxString wxRecord(record.c_str(), wxConvUTF8);
                            MainEditBox->AppendText(wxRecord);
                            
                            //Reset record variable
                            record = "";
                        }
                        else if (maj == "ELET") {
                            record = makeRecord(num, first, last, maj, score);
                                    
                            record.append("\t\t...Enqueued at Tail\n");
                            dequeList.enqueueTail(num, first, last, maj, score);
                                                    
                            wxString wxRecord(record.c_str(), wxConvUTF8);
                            MainEditBox->AppendText(wxRecord);
                            
                            //Reset record variable
                            record = "";
                        }
                    }
            }  // End while
}

void ProjectFrame::OnAddHeadDeque(wxCommandEvent& event)
{
    if (TRUE) {
        long    num;
        string  first;
        string  last;
        string  maj;
        float   score;

        string holder = "";
        string record;

        DataDialog* dlg = new DataDialog( wxT("COMP2611 - Data Entry"), 
               wxPoint(200,200), wxSize(600,300) );

        if (dlg->ShowModal() == wxID_OK) {
            num = dlg->IdNoEditBox->GetValue();
            first = dlg->NameEditBox->GetValue();
            last = dlg->SurnameEditBox->GetValue();
            maj = dlg->majorCombo->GetValue();
            score = dlg->GPAEditBox->GetValue();

            if (maj == "COMP") {
                queueList.enqueue(num, first, last, maj, score);
                priorityQueueList.enqueue(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                avlList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "IT") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                stackList.push(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                rbList.insert(num, first, last, maj, score);
                btreeList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "MATH") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                dequeList.enqueueHead(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                splayList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "ELET") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                dequeList.enqueueTail(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "PHYS") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            }

            OnDisplayDeque(event);
        }
        else {
          dlg->Close();
        }
        dlg->Destroy();
    }
}

void ProjectFrame::OnAddTailDeque(wxCommandEvent& event)
{
    if (TRUE) {
        long    num;
        string  first;
        string  last;
        string  maj;
        float   score;

        string holder = "";
        string record;

        DataDialog* dlg = new DataDialog( wxT("COMP2611 - Data Entry"), 
               wxPoint(200,200), wxSize(600,300) );

        if (dlg->ShowModal() == wxID_OK) {
            num = dlg->IdNoEditBox->GetValue();
            first = dlg->NameEditBox->GetValue();
            last = dlg->SurnameEditBox->GetValue();
            maj = dlg->majorCombo->GetValue();
            score = dlg->GPAEditBox->GetValue();

            if (maj == "COMP") {
                queueList.enqueue(num, first, last, maj, score);
                priorityQueueList.enqueue(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                avlList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "IT") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                stackList.push(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                rbList.insert(num, first, last, maj, score);
                btreeList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "MATH") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                dequeList.enqueueTail(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                splayList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "ELET") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                dequeList.enqueueTail(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "PHYS") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            }

            OnDisplayDeque(event);
        }
        else {
          dlg->Close();
        }
        dlg->Destroy();
    }
}

void ProjectFrame::OnDisplayDeque(wxCommandEvent& event)
  {
    if (!dequeList.isEmpty()) {
      // Clear the edit box
      MainEditBox->Clear();     

      string records = dequeList.showNodes();
        
      //Convert to a wxString
      wxString wxRecords(records.c_str(), wxConvUTF8);

      //display the words in the MainEditBox
      MainEditBox->AppendText(wxT("\t\t\t\t\nRecords in Deque:\n\n"));
      MainEditBox->AppendText(wxRecords);
    } else {
        wxMessageBox(wxT("No records are saved in this ADT!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
    }
  }

void ProjectFrame::OnShowHeadDeque(wxCommandEvent& event)
{
    string heading, records;

    if (!dequeList.isEmpty())
    {
        heading = "IDNo \tName \t\t\tSurname \t\tMajor \t\tGPA\n";
        heading.append("=====================================================\n");

        records = dequeList.showHead();

        // Clear the edit box
        MainEditBox->Clear();
        MainEditBox->AppendText(heading);
        MainEditBox->AppendText(records);
    } else {
        wxMessageBox(wxT("No records are saved in this ADT!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
    }
}

void ProjectFrame::OnShowTailDeque(wxCommandEvent& event)
{
    string heading, records;

    if (!dequeList.isEmpty())
    {
        heading = "IDNo \tName \t\t\tSurname \t\tMajor \t\tGPA\n";
        heading.append("=====================================================\n");

        records = dequeList.showTail();

        // Clear the edit box
        MainEditBox->Clear();
        MainEditBox->AppendText(heading);
        MainEditBox->AppendText(records);
    } else {
        wxMessageBox(wxT("No records are saved in this ADT!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
    }
}

void ProjectFrame::OnDequeueHeadDeque(wxCommandEvent& event)
{
    long    num;
    string  first;
    string  last;
    string  maj;
    float   score;

    string holder = "";
    string record;
    string heading;

    if (!dequeList.isEmpty())
    {
        heading = "Record Removed from Deque\n\n";
        heading.append("IDNo \tName \t\t\tSurname \t\tMajor \t\tGPA\n");
        heading.append("=====================================================\n");

        holder = dequeList.dequeueHead();

        stringstream record(holder);
        record >> num >> first >> last >> maj >> score;

        queueList.removeTarget(num);
        dequeList.removeTarget(num);
        priorityQueueList.removeTarget(num);
        stackList.removeTarget(num);
        bstList.remove(num);
        avlList.remove(num);
        rbList.remove(num);
        //heapList.remove(num);
        splayList.remove(num);
        //btreeList.remove(num);
        setAList.delNode(num);
        setBList.delNode(num);

        // Clear the edit box
        MainEditBox->Clear();
        MainEditBox->AppendText(heading);
        MainEditBox->AppendText(holder);
    } else {
        wxMessageBox(wxT("No records are saved in this ADT!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
    }
}

void ProjectFrame::OnDequeueTailDeque(wxCommandEvent& event)
{
    long    num;
    string  first;
    string  last;
    string  maj;
    float   score;

    string holder = "";
    string record;
    string heading;

    if (!dequeList.isEmpty())
    {
        heading = "Record Removed from Deque\n\n";
        heading.append("IDNo \tName \t\t\tSurname \t\tMajor \t\tGPA\n");
        heading.append("=====================================================\n");

        holder = dequeList.dequeueTail();

        stringstream record(holder);
        record >> num >> first >> last >> maj >> score;

        queueList.removeTarget(num);
        dequeList.removeTarget(num);
        priorityQueueList.removeTarget(num);
        stackList.removeTarget(num);
        bstList.remove(num);
        avlList.remove(num);
        rbList.remove(num);
        //heapList.remove(num);
        splayList.remove(num);
        //btreeList.remove(num);
        setAList.delNode(num);
        setBList.delNode(num);

        // Clear the edit box
        MainEditBox->Clear();
        MainEditBox->AppendText(heading);
        MainEditBox->AppendText(holder);
    } else {
        wxMessageBox(wxT("No records are saved in this ADT!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
    }
}

//=============================================================================
//============== END Definition for the Deque Functions =======================
//=============================================================================

//=============================================================================
//============== Definition for the Priority Queue Functions ==================
//=============================================================================

void ProjectFrame::OnCreatePriorityQueue(wxCommandEvent& event)
{
        long    num;
        string  first;
        string  last;
        string  maj;
        float   score;
 //       string  line;

        string record, heading, dummyString;
        
    // Clear the edit box
    MainEditBox->Clear();

    //Empty the Queue
    priorityQueueList.purgePriorityQueue();   // Write that function!
      
    //open the file
    ifstream infile(CurrentDocPath.mb_str(), ios::in);

    // Check to see if a valid file was opened.
    if (!infile)
      {
          MainEditBox->AppendText(wxT("\n\n\t\t\t\tNo Data file opened as yet...\n\n"));
        return;
      }
    
        // Read off the heading line
        infile >> dummyString >> dummyString >> dummyString >> dummyString >> dummyString;
  
        // Create a label for the displayed records
        heading = "IDNo \tName \t\t\tSurname \t\tMajor \t\tGPA\n";
        heading.append("=====================================================\n");

        
        // Convert "heading" into a wxString and display it in the MainEditBox
        wxString headingLine(heading.c_str(), wxConvUTF8);
        MainEditBox->AppendText(headingLine);

        while (!infile.eof())
            {
                infile >> num >> first >> last >> maj >> score;
 
                if (!infile.eof())
                    {
                            record = makeRecord(num, first, last, maj, score);
                                    
                            record.append("\t\t...Enqueued\n");
                            priorityQueueList.enqueue(num, first, last, maj, score);
                                                    
                            wxString wxRecord(record.c_str(), wxConvUTF8);
                            MainEditBox->AppendText(wxRecord);
                            
                            //Reset record variable
                            record = "";
                        
                    }
            }  // End while
}

void ProjectFrame::OnAddPriorityQueue(wxCommandEvent& event)
{
    if (TRUE) {
        long    num;
        string  first;
        string  last;
        string  maj;
        float   score;

        string holder = "";
        string record;

        DataDialog* dlg = new DataDialog( wxT("COMP2611 - Data Entry"), 
               wxPoint(200,200), wxSize(600,300) );

        if (dlg->ShowModal() == wxID_OK) {
            num = dlg->IdNoEditBox->GetValue();
            first = dlg->NameEditBox->GetValue();
            last = dlg->SurnameEditBox->GetValue();
            maj = dlg->majorCombo->GetValue();
            score = dlg->GPAEditBox->GetValue();

            if (maj == "COMP") {
                queueList.enqueue(num, first, last, maj, score);
                priorityQueueList.enqueue(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                avlList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "IT") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                stackList.push(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                rbList.insert(num, first, last, maj, score);
                btreeList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "MATH") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                dequeList.enqueueHead(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                splayList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "ELET") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                dequeList.enqueueTail(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "PHYS") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            }

            OnDisplayPriorityQueue(event);
        }
        else {
          dlg->Close();
        }
        dlg->Destroy();
    }
}

void ProjectFrame::OnDisplayPriorityQueue(wxCommandEvent& event)
  {
    if (!priorityQueueList.isEmpty()) {
      // Clear the edit box
      MainEditBox->Clear();     

      string records = priorityQueueList.showNodes();
        
      //Convert to a wxString
      wxString wxRecords(records.c_str(), wxConvUTF8);

      //display the words in the MainEditBox
      MainEditBox->AppendText(wxT("\t\t\t\t\nRecords in Priority Queue:\n\n"));
      MainEditBox->AppendText(wxRecords);
    } else {
        wxMessageBox(wxT("No records are saved in this ADT!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
    }
  }

void ProjectFrame::OnShowHeadPriorityQueue(wxCommandEvent& event)
{
    string heading, records;

    if (!priorityQueueList.isEmpty())
    {
        heading = "IDNo \tName \t\t\tSurname \t\tMajor \t\tGPA\n";
        heading.append("=====================================================\n");

        records = priorityQueueList.showHead();

        // Clear the edit box
        MainEditBox->Clear();
        MainEditBox->AppendText(heading);
        MainEditBox->AppendText(records);
    } else {
        wxMessageBox(wxT("No records are saved in this ADT!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
    }
}

void ProjectFrame::OnShowTailPriorityQueue(wxCommandEvent& event)
{
    string heading, records;

    if (!priorityQueueList.isEmpty())
    {
        heading = "IDNo \tName \t\t\tSurname \t\tMajor \t\tGPA\n";
        heading.append("=====================================================\n");

        records = priorityQueueList.showTail();

        // Clear the edit box
        MainEditBox->Clear();
        MainEditBox->AppendText(heading);
        MainEditBox->AppendText(records);
    } else {
        wxMessageBox(wxT("No records are saved in this ADT!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
    }
}

void ProjectFrame::OnDequeuePriorityQueue(wxCommandEvent& event)
{
    long    num;
    string  first;
    string  last;
    string  maj;
    float   score;

    string holder = "";
    string record;
    string heading;

    if (!priorityQueueList.isEmpty())
    {
        heading = "Record Removed from Priority Queue\n\n";
        heading.append("IDNo \tName \t\t\tSurname \t\tMajor \t\tGPA\n");
        heading.append("=====================================================\n");

        holder = priorityQueueList.dequeue();

        stringstream record(holder);
        record >> num >> first >> last >> maj >> score;

        queueList.removeTarget(num);
        dequeList.removeTarget(num);
        priorityQueueList.removeTarget(num);
        stackList.removeTarget(num);
        bstList.remove(num);
        avlList.remove(num);
        rbList.remove(num);
        //heapList.remove(num);
        splayList.remove(num);
        //btreeList.remove(num);
        setAList.delNode(num);
        setBList.delNode(num);

        // Clear the edit box
        MainEditBox->Clear();
        MainEditBox->AppendText(heading);
        MainEditBox->AppendText(holder);
    } else {
        wxMessageBox(wxT("No records are saved in this ADT!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
    }
}

//=============================================================================
//============== END Definition for the Priority Queue Functions ==============
//=============================================================================

//=============================================================================
//============== Definition for the Stack Functions ===========================
//=============================================================================

void ProjectFrame::OnCreateStack(wxCommandEvent& event)
{
        long    num;
        string  first;
        string  last;
        string  maj;
        float   score;
 //       string  line;

        string record, heading, dummyString;
        
    // Clear the edit box
    MainEditBox->Clear();

    //Empty the Queue
    stackList.purgeStack();   // Write that function!
      
    //open the file
    ifstream infile(CurrentDocPath.mb_str(), ios::in);

    // Check to see if a valid file was opened.
    if (!infile)
      {
          MainEditBox->AppendText(wxT("\n\n\t\t\t\tNo Data file opened as yet...\n\n"));
        return;
      }
    
        // Read off the heading line
        infile >> dummyString >> dummyString >> dummyString >> dummyString >> dummyString;
  
        // Create a label for the displayed records
        heading = "IDNo \tName \t\t\tSurname \t\tMajor \t\tGPA\n";
        heading.append("=====================================================\n");

        
        // Convert "heading" into a wxString and display it in the MainEditBox
        wxString headingLine(heading.c_str(), wxConvUTF8);
        MainEditBox->AppendText(headingLine);

        while (!infile.eof())
            {
                infile >> num >> first >> last >> maj >> score;
 
                if (!infile.eof())
                    {
                        if (maj == "IT") {
                            record = makeRecord(num, first, last, maj, score);
                                    
                            record.append("\t\t...Pushed\n");
                            stackList.push(num, first, last, maj, score);
                                                    
                            wxString wxRecord(record.c_str(), wxConvUTF8);
                            MainEditBox->AppendText(wxRecord);
                            
                            //Reset record variable
                            record = "";
                        }
                    }
            }  // End while
}

void ProjectFrame::OnPopStack(wxCommandEvent& event)
{
    long    num;
    string  first;
    string  last;
    string  maj;
    float   score;

    string holder = "";
    string record;
    string heading;

    if (!stackList.isEmpty())
    {
        heading = "Record Removed from Stack\n\n";
        heading.append("IDNo \tName \t\t\tSurname \t\tMajor \t\tGPA\n");
        heading.append("=====================================================\n");

        holder = stackList.pop();

        stringstream record(holder);
        record >> num >> first >> last >> maj >> score;

        queueList.removeTarget(num);
        dequeList.removeTarget(num);
        priorityQueueList.removeTarget(num);
        stackList.removeTarget(num);
        bstList.remove(num);
        avlList.remove(num);
        rbList.remove(num);
        //heapList.remove(num);
        splayList.remove(num);
        //btreeList.remove(num);
        setAList.delNode(num);
        setBList.delNode(num);

        // Clear the edit box
        MainEditBox->Clear();
        MainEditBox->AppendText(heading);
        MainEditBox->AppendText(holder);
    } else {
        wxMessageBox(wxT("No records are saved in this ADT!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
    }
}

void ProjectFrame::OnPushStack(wxCommandEvent& event)
{
    if (TRUE) {
        long    num;
        string  first;
        string  last;
        string  maj;
        float   score;

        string holder = "";
        string record;

        DataDialog* dlg = new DataDialog( wxT("COMP2611 - Data Entry"), 
               wxPoint(200,200), wxSize(600,300) );

        if (dlg->ShowModal() == wxID_OK) {
            num = dlg->IdNoEditBox->GetValue();
            first = dlg->NameEditBox->GetValue();
            last = dlg->SurnameEditBox->GetValue();
            maj = dlg->majorCombo->GetValue();
            score = dlg->GPAEditBox->GetValue();

            if (maj == "COMP") {
                queueList.enqueue(num, first, last, maj, score);
                priorityQueueList.enqueue(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                avlList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "IT") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                stackList.push(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                rbList.insert(num, first, last, maj, score);
                btreeList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "MATH") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                dequeList.enqueueHead(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                splayList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "ELET") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                dequeList.enqueueTail(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "PHYS") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            }

            OnDisplayStack(event);
        }
        else {
          dlg->Close();
        }
        dlg->Destroy();
    }
}

void ProjectFrame::OnDisplayStack(wxCommandEvent& event)
  {
    if (!stackList.isEmpty()) {
      // Clear the edit box
      MainEditBox->Clear();     

      string records = stackList.showNodes();
        
      //Convert to a wxString
      wxString wxRecords(records.c_str(), wxConvUTF8);

      //display the words in the MainEditBox
      MainEditBox->AppendText(wxT("\t\t\t\t\nRecords in Stack:\n\n"));
      MainEditBox->AppendText(wxRecords);
    } else {
        wxMessageBox(wxT("No records are saved in this ADT!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
    }
  }

//=============================================================================
//============== END Definition for the Stack Functions =======================
//=============================================================================

//=============================================================================
//============== Definition for the BST Functions =============================
//=============================================================================

void ProjectFrame::OnCreateBST(wxCommandEvent& event)
{
        long    num;
        string  first;
        string  last;
        string  maj;
        float   score;
 //       string  line;

        string record, heading, dummyString;
        
    // Clear the edit box
    MainEditBox->Clear();

    //Empty the BST
    bstList.purgeBST();   // Write that function!
      
    //open the file
    ifstream infile(CurrentDocPath.mb_str(), ios::in);

    // Check to see if a valid file was opened.
    if (!infile)
      {
          MainEditBox->AppendText(wxT("\n\n\t\t\t\tNo Data file opened as yet...\n\n"));
        return;
      }
    
        // Read off the heading line
        infile >> dummyString >> dummyString >> dummyString >> dummyString >> dummyString;
  
        // Create a label for the displayed records
        heading = "IDNo \tName \t\t\tSurname \t\tMajor \t\tGPA\n";
        heading.append("=====================================================\n");

        
        // Convert "heading" into a wxString and display it in the MainEditBox
        wxString headingLine(heading.c_str(), wxConvUTF8);
        MainEditBox->AppendText(headingLine);

        while (!infile.eof())
            {
                infile >> num >> first >> last >> maj >> score;
 
                if (!infile.eof())
                    {
                        record = makeRecord(num, first, last, maj, score);
                                   
                        record.append("\t\t...Inserted\n");
                        bstList.insert(num, first, last, maj, score);
                                                    
                        wxString wxRecord(record.c_str(), wxConvUTF8);
                        MainEditBox->AppendText(wxRecord);
                            
                        //Reset record variable
                        record = "";
                    }
            }  // End while
}

void ProjectFrame::OnAddBST(wxCommandEvent& event)
{
    if (TRUE) {
        long    num;
        string  first;
        string  last;
        string  maj;
        float   score;

        string holder = "";
        string record;

        DataDialog* dlg = new DataDialog( wxT("COMP2611 - Data Entry"), 
               wxPoint(200,200), wxSize(600,300) );

        if (dlg->ShowModal() == wxID_OK) {
            num = dlg->IdNoEditBox->GetValue();
            first = dlg->NameEditBox->GetValue();
            last = dlg->SurnameEditBox->GetValue();
            maj = dlg->majorCombo->GetValue();
            score = dlg->GPAEditBox->GetValue();

            if (maj == "COMP") {
                queueList.enqueue(num, first, last, maj, score);
                priorityQueueList.enqueue(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                avlList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "IT") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                stackList.push(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                rbList.insert(num, first, last, maj, score);
                btreeList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "MATH") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                dequeList.enqueueHead(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                splayList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "ELET") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                dequeList.enqueueTail(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "PHYS") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            }

            OnInorderBST(event);
        }
        else {
          dlg->Close();
        }
        dlg->Destroy();
    }
}

void ProjectFrame::OnDeleteBST(wxCommandEvent& event)
{
    if (TRUE) {
        long    num;
        string  first;
        string  last;
        string  maj;
        float   score;

        string holder = "";
        string record;

        wxTextEntryDialog dlg(this, "Enter Data of the Record to be removed");

        if (dlg.ShowModal() == wxID_OK) {
            wxMessageBox(holder = dlg.GetValue());
            stringstream record(holder);
            record >> num;

            queueList.removeTarget(num);
            dequeList.removeTarget(num);
            priorityQueueList.removeTarget(num);
            stackList.removeTarget(num);
            bstList.remove(num);
            avlList.remove(num);
            heapList.remove(num);
            rbList.remove(num);
            splayList.remove(num);
            setAList.delNode(num);
            setBList.delNode(num);
        }
    }
}

void ProjectFrame::OnInorderBST(wxCommandEvent& event)
  {
    if (!bstList.isEmpty()) {
      // Clear the edit box
      MainEditBox->Clear();     

      string records = bstList.inorder();
        
      //Convert to a wxString
      wxString wxRecords(records.c_str(), wxConvUTF8);

      //display the words in the MainEditBox
      MainEditBox->AppendText(wxT("\t\t\t\t\nRecords in BST Inorder:\n\n"));
      MainEditBox->AppendText(wxRecords);
    } else {
        wxMessageBox(wxT("No records are saved in this ADT!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
    }
  }

void ProjectFrame::OnPreorderBST(wxCommandEvent& event)
  {
    if (!bstList.isEmpty()) {
      // Clear the edit box
      MainEditBox->Clear();     

      string records = bstList.preorder();
        
      //Convert to a wxString
      wxString wxRecords(records.c_str(), wxConvUTF8);

      //display the words in the MainEditBox
      MainEditBox->AppendText(wxT("\t\t\t\t\nRecords in BST Preorder:\n\n"));
      MainEditBox->AppendText(wxRecords);
    } else {
        wxMessageBox(wxT("No records are saved in this ADT!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
    }
  }

void ProjectFrame::OnPostorderBST(wxCommandEvent& event)
  {
    if (!bstList.isEmpty()) {
      // Clear the edit box
      MainEditBox->Clear();     

      string records = bstList.postorder();
        
      //Convert to a wxString
      wxString wxRecords(records.c_str(), wxConvUTF8);

      //display the words in the MainEditBox
      MainEditBox->AppendText(wxT("\t\t\t\t\nRecords in BST Postorder:\n\n"));
      MainEditBox->AppendText(wxRecords);
    } else {
        wxMessageBox(wxT("No records are saved in this ADT!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
    }
  }

//=============================================================================
//============== END Definition for the BST Functions =========================
//=============================================================================

//=============================================================================
//============== Definition for the AVL Functions =============================
//=============================================================================

void ProjectFrame::OnCreateAVL(wxCommandEvent& event)
{
        long    num;
        string  first;
        string  last;
        string  maj;
        float   score;
 //       string  line;

        string record, heading, dummyString;
        
    // Clear the edit box
    MainEditBox->Clear();

    //Empty the BST
    avlList.purgeAVL();   // Write that function!
      
    //open the file
    ifstream infile(CurrentDocPath.mb_str(), ios::in);

    // Check to see if a valid file was opened.
    if (!infile)
      {
          MainEditBox->AppendText(wxT("\n\n\t\t\t\tNo Data file opened as yet...\n\n"));
        return;
      }
    
        // Read off the heading line
        infile >> dummyString >> dummyString >> dummyString >> dummyString >> dummyString;
  
        // Create a label for the displayed records
        heading = "IDNo \tName \t\t\tSurname \t\tMajor \t\tGPA\n";
        heading.append("=====================================================\n");

        
        // Convert "heading" into a wxString and display it in the MainEditBox
        wxString headingLine(heading.c_str(), wxConvUTF8);
        MainEditBox->AppendText(headingLine);

        while (!infile.eof())
            {
                infile >> num >> first >> last >> maj >> score;
 
                if (!infile.eof())
                    {
                        if (maj == "COMP") {
                            record = makeRecord(num, first, last, maj, score);
                                       
                            record.append("\t\t...Inserted\n");
                            avlList.insert(num, first, last, maj, score);
                                                        
                            wxString wxRecord(record.c_str(), wxConvUTF8);
                            MainEditBox->AppendText(wxRecord);
                                
                            //Reset record variable
                            record = "";
                        }
                    }
            }  // End while
}

void ProjectFrame::OnAddAVL(wxCommandEvent& event)
{
    if (TRUE) {
        long    num;
        string  first;
        string  last;
        string  maj;
        float   score;

        string holder = "";
        string record;

        DataDialog* dlg = new DataDialog( wxT("COMP2611 - Data Entry"), 
               wxPoint(200,200), wxSize(600,300) );

        if (dlg->ShowModal() == wxID_OK) {
            num = dlg->IdNoEditBox->GetValue();
            first = dlg->NameEditBox->GetValue();
            last = dlg->SurnameEditBox->GetValue();
            maj = dlg->majorCombo->GetValue();
            score = dlg->GPAEditBox->GetValue();

            if (maj == "COMP") {
                queueList.enqueue(num, first, last, maj, score);
                priorityQueueList.enqueue(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                avlList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "IT") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                stackList.push(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                rbList.insert(num, first, last, maj, score);
                btreeList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "MATH") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                dequeList.enqueueHead(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                splayList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "ELET") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                dequeList.enqueueTail(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "PHYS") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            }

            OnInorderAVL(event);
        }
        else {
          dlg->Close();
        }
        dlg->Destroy();
    }
}

void ProjectFrame::OnDeleteAVL(wxCommandEvent& event)
{
    if (TRUE) {
        long    num;
        string  first;
        string  last;
        string  maj;
        float   score;

        string holder = "";
        string record;

        wxTextEntryDialog dlg(this, "Enter Data of the Record to be removed");

        if (dlg.ShowModal() == wxID_OK) {
            wxMessageBox(holder = dlg.GetValue());
            stringstream record(holder);
            record >> num;

            queueList.removeTarget(num);
            dequeList.removeTarget(num);
            priorityQueueList.removeTarget(num);
            stackList.removeTarget(num);
            bstList.remove(num);
            avlList.remove(num);
            heapList.remove(num);
            rbList.remove(num);
            splayList.remove(num);
            setAList.delNode(num);
            setBList.delNode(num);
        }
    }
}

void ProjectFrame::OnInorderAVL(wxCommandEvent& event)
  {
    if (!avlList.isEmpty()) {
      // Clear the edit box
      MainEditBox->Clear();     

      string records = avlList.inorder();
        
      //Convert to a wxString
      wxString wxRecords(records.c_str(), wxConvUTF8);

      //display the words in the MainEditBox
      MainEditBox->AppendText(wxT("\t\t\t\t\nRecords in AVL Inorder:\n\n"));
      MainEditBox->AppendText(wxRecords);
    } else {
        wxMessageBox(wxT("No records are saved in this ADT!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
    }
  }

void ProjectFrame::OnPreorderAVL(wxCommandEvent& event)
  {
    if (!avlList.isEmpty()) {
      // Clear the edit box
      MainEditBox->Clear();     

      string records = avlList.preorder();
        
      //Convert to a wxString
      wxString wxRecords(records.c_str(), wxConvUTF8);

      //display the words in the MainEditBox
      MainEditBox->AppendText(wxT("\t\t\t\t\nRecords in AVL Preorder:\n\n"));
      MainEditBox->AppendText(wxRecords);
    } else {
        wxMessageBox(wxT("No records are saved in this ADT!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
    }
  }

void ProjectFrame::OnPostorderAVL(wxCommandEvent& event)
  {
    if (!avlList.isEmpty()) {
      // Clear the edit box
      MainEditBox->Clear();     

      string records = avlList.postorder();
        
      //Convert to a wxString
      wxString wxRecords(records.c_str(), wxConvUTF8);

      //display the words in the MainEditBox
      MainEditBox->AppendText(wxT("\t\t\t\t\nRecords in AVL Postorder:\n\n"));
      MainEditBox->AppendText(wxRecords);
    } else {
        wxMessageBox(wxT("No records are saved in this ADT!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
    }
  }

//=============================================================================
//============== END Definition for the AVL Functions =========================
//=============================================================================

//=============================================================================
//============== Definition for the Heap Functions ============================
//=============================================================================

  void ProjectFrame::OnCreateHeap(wxCommandEvent& event)
{
        long    num;
        string  first;
        string  last;
        string  maj;
        float   score;
 //       string  line;

        string record, heading, dummyString;
        
    // Clear the edit box
    MainEditBox->Clear();

    //Empty the BST
    heapList.purgeHeap();   // Write that function!
      
    //open the file
    ifstream infile(CurrentDocPath.mb_str(), ios::in);

    // Check to see if a valid file was opened.
    if (!infile)
      {
          MainEditBox->AppendText(wxT("\n\n\t\t\t\tNo Data file opened as yet...\n\n"));
        return;
      }
    
        // Read off the heading line
        infile >> dummyString >> dummyString >> dummyString >> dummyString >> dummyString;
  
        // Create a label for the displayed records
        heading = "IDNo \tName \t\t\tSurname \t\tMajor \t\tGPA\n";
        heading.append("=====================================================\n");

        
        // Convert "heading" into a wxString and display it in the MainEditBox
        wxString headingLine(heading.c_str(), wxConvUTF8);
        MainEditBox->AppendText(headingLine);

        while (!infile.eof())
            {
                infile >> num >> first >> last >> maj >> score;
 
                if (!infile.eof())
                    {
                        if (maj == "COMP" || maj == "MATH" || maj == "ELET") {
                            record = makeRecord(num, first, last, maj, score);
                                       
                            record.append("\t\t...Inserted\n");
                            heapList.insert(num, first, last, maj, score);
                                                        
                            wxString wxRecord(record.c_str(), wxConvUTF8);
                            MainEditBox->AppendText(wxRecord);
                                
                            //Reset record variable
                            record = "";
                        }
                    }
            }  // End while
}

void ProjectFrame::OnAddHeap(wxCommandEvent& event)
{
    if (TRUE) {
        long    num;
        string  first;
        string  last;
        string  maj;
        float   score;

        string holder = "";
        string record;

        DataDialog* dlg = new DataDialog( wxT("COMP2611 - Data Entry"), 
               wxPoint(200,200), wxSize(600,300) );

        if (dlg->ShowModal() == wxID_OK) {
            num = dlg->IdNoEditBox->GetValue();
            first = dlg->NameEditBox->GetValue();
            last = dlg->SurnameEditBox->GetValue();
            maj = dlg->majorCombo->GetValue();
            score = dlg->GPAEditBox->GetValue();

            if (maj == "COMP") {
                queueList.enqueue(num, first, last, maj, score);
                priorityQueueList.enqueue(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                avlList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "IT") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                stackList.push(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                rbList.insert(num, first, last, maj, score);
                btreeList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "MATH") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                dequeList.enqueueHead(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                splayList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "ELET") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                dequeList.enqueueTail(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "PHYS") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            }

            OnDisplayHeap(event);
        }
        else {
          dlg->Close();
        }
        dlg->Destroy();
    }
}

void ProjectFrame::OnDeleteHeap(wxCommandEvent& event)
{
    if (TRUE) {
        long    num;
        string  first;
        string  last;
        string  maj;
        float   score;

        string holder = "";
        string record;

        wxTextEntryDialog dlg(this, "Enter Data of the Record to be removed");

        if (dlg.ShowModal() == wxID_OK) {
            wxMessageBox(holder = dlg.GetValue());
            stringstream record(holder);
            record >> num;

            queueList.removeTarget(num);
            dequeList.removeTarget(num);
            priorityQueueList.removeTarget(num);
            stackList.removeTarget(num);
            bstList.remove(num);
            avlList.remove(num);
            heapList.remove(num);
            rbList.remove(num);
            splayList.remove(num);
            setAList.delNode(num);
            setBList.delNode(num);
        }
    }
}

void ProjectFrame::OnDisplayHeap(wxCommandEvent& event)
  {
    if (!heapList.isEmpty()) {
      // Clear the edit box
      MainEditBox->Clear();     

      string records = heapList.inorder();
        
      //Convert to a wxString
      wxString wxRecords(records.c_str(), wxConvUTF8);

      //display the words in the MainEditBox
      MainEditBox->AppendText(wxT("\t\t\t\t\nRecords in Heap:\n\n"));
      MainEditBox->AppendText(wxRecords);
    } else {
        wxMessageBox(wxT("No records are saved in this ADT!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
    }
  }

//=============================================================================
//============== END Definition for the Heap Functions ========================
//=============================================================================

//=============================================================================
//============== Definition for the RB Tree Functions =========================
//=============================================================================

void ProjectFrame::OnCreateRB(wxCommandEvent& event)
{
        long    num;
        string  first;
        string  last;
        string  maj;
        float   score;
 //       string  line;

        string record, heading, dummyString;
        
    // Clear the edit box
    MainEditBox->Clear();

    //Empty the BST
    rbList.purgeRB();   // Write that function!
      
    //open the file
    ifstream infile(CurrentDocPath.mb_str(), ios::in);

    // Check to see if a valid file was opened.
    if (!infile)
      {
          MainEditBox->AppendText(wxT("\n\n\t\t\t\tNo Data file opened as yet...\n\n"));
        return;
      }
    
        // Read off the heading line
        infile >> dummyString >> dummyString >> dummyString >> dummyString >> dummyString;
  
        // Create a label for the displayed records
        heading = "IDNo \tName \t\t\tSurname \t\tMajor \t\tGPA\n";
        heading.append("=====================================================\n");

        
        // Convert "heading" into a wxString and display it in the MainEditBox
        wxString headingLine(heading.c_str(), wxConvUTF8);
        MainEditBox->AppendText(headingLine);

        while (!infile.eof())
            {
                infile >> num >> first >> last >> maj >> score;
 
                if (!infile.eof())
                    {
                        if (maj == "COMP") {
                            record = makeRecord(num, first, last, maj, score);
                                       
                            record.append("\t\t...Inserted\n");
                            rbList.insert(num, first, last, maj, score);
                                                        
                            wxString wxRecord(record.c_str(), wxConvUTF8);
                            MainEditBox->AppendText(wxRecord);
                                
                            //Reset record variable
                            record = "";
                        }
                    }
            }  // End while
}

void ProjectFrame::OnAddRB(wxCommandEvent& event)
{
    if (TRUE) {
        long    num;
        string  first;
        string  last;
        string  maj;
        float   score;

        string holder = "";
        string record;

        DataDialog* dlg = new DataDialog( wxT("COMP2611 - Data Entry"), 
               wxPoint(200,200), wxSize(600,300) );

        if (dlg->ShowModal() == wxID_OK) {
            num = dlg->IdNoEditBox->GetValue();
            first = dlg->NameEditBox->GetValue();
            last = dlg->SurnameEditBox->GetValue();
            maj = dlg->majorCombo->GetValue();
            score = dlg->GPAEditBox->GetValue();

            if (maj == "COMP") {
                queueList.enqueue(num, first, last, maj, score);
                priorityQueueList.enqueue(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                avlList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "IT") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                stackList.push(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                rbList.insert(num, first, last, maj, score);
                btreeList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "MATH") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                dequeList.enqueueHead(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                splayList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "ELET") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                dequeList.enqueueTail(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "PHYS") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            }

            OnInorderRB(event);
        }
        else {
          dlg->Close();
        }
        dlg->Destroy();
    }
}

void ProjectFrame::OnDeleteRB(wxCommandEvent& event)
{
    if (TRUE) {
        long    num;
        string  first;
        string  last;
        string  maj;
        float   score;

        string holder = "";
        string record;

        wxTextEntryDialog dlg(this, "Enter Data of the Record to be removed");

        if (dlg.ShowModal() == wxID_OK) {
            wxMessageBox(holder = dlg.GetValue());
            stringstream record(holder);
            record >> num;

            queueList.removeTarget(num);
            dequeList.removeTarget(num);
            priorityQueueList.removeTarget(num);
            stackList.removeTarget(num);
            bstList.remove(num);
            avlList.remove(num);
            heapList.remove(num);
            rbList.remove(num);
            splayList.remove(num);
            setAList.delNode(num);
            setBList.delNode(num);
        }
    }
}

void ProjectFrame::OnInorderRB(wxCommandEvent& event)
  {
    if (!rbList.isEmpty()) {
      // Clear the edit box
      MainEditBox->Clear();     

      string records = rbList.inorder();
        
      //Convert to a wxString
      wxString wxRecords(records.c_str(), wxConvUTF8);

      //display the words in the MainEditBox
      MainEditBox->AppendText(wxT("\t\t\t\t\nRecords in RB Tree Inorder:\n\n"));
      MainEditBox->AppendText(wxRecords);
    } else {
        wxMessageBox(wxT("No records are saved in this ADT!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
    }
  }

void ProjectFrame::OnPreorderRB(wxCommandEvent& event)
  {
    if (!rbList.isEmpty()) {
      // Clear the edit box
      MainEditBox->Clear();     

      string records = rbList.preorder();
        
      //Convert to a wxString
      wxString wxRecords(records.c_str(), wxConvUTF8);

      //display the words in the MainEditBox
      MainEditBox->AppendText(wxT("\t\t\t\t\nRecords in RB Tree Preorder:\n\n"));
      MainEditBox->AppendText(wxRecords);
    } else {
        wxMessageBox(wxT("No records are saved in this ADT!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
    }
  }

void ProjectFrame::OnPostorderRB(wxCommandEvent& event)
  {
    if (!rbList.isEmpty()) {
      // Clear the edit box
      MainEditBox->Clear();     

      string records = rbList.postorder();
        
      //Convert to a wxString
      wxString wxRecords(records.c_str(), wxConvUTF8);

      //display the words in the MainEditBox
      MainEditBox->AppendText(wxT("\t\t\t\t\nRecords in RB Tree Postorder:\n\n"));
      MainEditBox->AppendText(wxRecords);
    } else {
        wxMessageBox(wxT("No records are saved in this ADT!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
    }
  }

//=============================================================================
//============== END Definition for the RB Tree Functions =====================
//=============================================================================

//=============================================================================
//============== Definition for the Splay Tree Functions ======================
//=============================================================================

void ProjectFrame::OnCreateSplay(wxCommandEvent& event)
{
        long    num;
        string  first;
        string  last;
        string  maj;
        float   score;
 //       string  line;

        string record, heading, dummyString;
        
    // Clear the edit box
    MainEditBox->Clear();

    //Empty the BST
    splayList.purgeSplay();   // Write that function!
      
    //open the file
    ifstream infile(CurrentDocPath.mb_str(), ios::in);

    // Check to see if a valid file was opened.
    if (!infile)
      {
          MainEditBox->AppendText(wxT("\n\n\t\t\t\tNo Data file opened as yet...\n\n"));
        return;
      }
    
        // Read off the heading line
        infile >> dummyString >> dummyString >> dummyString >> dummyString >> dummyString;
  
        // Create a label for the displayed records
        heading = "IDNo \tName \t\t\tSurname \t\tMajor \t\tGPA\n";
        heading.append("=====================================================\n");

        
        // Convert "heading" into a wxString and display it in the MainEditBox
        wxString headingLine(heading.c_str(), wxConvUTF8);
        MainEditBox->AppendText(headingLine);

        while (!infile.eof())
            {
                infile >> num >> first >> last >> maj >> score;
 
                if (!infile.eof())
                    {
                        if (maj == "MATH") {
                            record = makeRecord(num, first, last, maj, score);
                                       
                            record.append("\t\t...Inserted\n");
                            splayList.insert(num, first, last, maj, score);
                                                        
                            wxString wxRecord(record.c_str(), wxConvUTF8);
                            MainEditBox->AppendText(wxRecord);
                                
                            //Reset record variable
                            record = "";
                        }
                    }
            }  // End while
}

void ProjectFrame::OnAddSplay(wxCommandEvent& event)
{
    if (TRUE) {
        long    num;
        string  first;
        string  last;
        string  maj;
        float   score;

        string holder = "";
        string record;

        DataDialog* dlg = new DataDialog( wxT("COMP2611 - Data Entry"), 
               wxPoint(200,200), wxSize(600,300) );

        if (dlg->ShowModal() == wxID_OK) {
            num = dlg->IdNoEditBox->GetValue();
            first = dlg->NameEditBox->GetValue();
            last = dlg->SurnameEditBox->GetValue();
            maj = dlg->majorCombo->GetValue();
            score = dlg->GPAEditBox->GetValue();

            if (maj == "COMP") {
                queueList.enqueue(num, first, last, maj, score);
                priorityQueueList.enqueue(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                avlList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "IT") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                stackList.push(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                rbList.insert(num, first, last, maj, score);
                btreeList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "MATH") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                dequeList.enqueueHead(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                splayList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "ELET") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                dequeList.enqueueTail(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "PHYS") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            }

            OnInorderSplay(event);
        }
        else {
          dlg->Close();
        }
        dlg->Destroy();
    }
}

void ProjectFrame::OnDeleteSplay(wxCommandEvent& event)
{
    if (TRUE) {
        long    num;
        string  first;
        string  last;
        string  maj;
        float   score;

        string holder = "";
        string record;

        wxTextEntryDialog dlg(this, "Enter Data of the Record to be removed");

        if (dlg.ShowModal() == wxID_OK) {
            wxMessageBox(holder = dlg.GetValue());
            stringstream record(holder);
            record >> num;

            queueList.removeTarget(num);
            dequeList.removeTarget(num);
            priorityQueueList.removeTarget(num);
            stackList.removeTarget(num);
            bstList.remove(num);
            avlList.remove(num);
            heapList.remove(num);
            rbList.remove(num);
            splayList.remove(num);
            setAList.delNode(num);
            setBList.delNode(num);
        }
    }
}

void ProjectFrame::OnInorderSplay(wxCommandEvent& event)
  {
    if (!splayList.isEmpty()) {
      // Clear the edit box
      MainEditBox->Clear();     

      string records = splayList.inorder();
        
      //Convert to a wxString
      wxString wxRecords(records.c_str(), wxConvUTF8);

      //display the words in the MainEditBox
      MainEditBox->AppendText(wxT("\t\t\t\t\nRecords in Splay Tree Inorder:\n\n"));
      MainEditBox->AppendText(wxRecords);
    } else {
        wxMessageBox(wxT("No records are saved in this ADT!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
    }
  }

void ProjectFrame::OnPreorderSplay(wxCommandEvent& event)
  {
    if (!splayList.isEmpty()) {
      // Clear the edit box
      MainEditBox->Clear();     

      string records = splayList.preorder();
        
      //Convert to a wxString
      wxString wxRecords(records.c_str(), wxConvUTF8);

      //display the words in the MainEditBox
      MainEditBox->AppendText(wxT("\t\t\t\t\nRecords in Splay Tree Preorder:\n\n"));
      MainEditBox->AppendText(wxRecords);
    } else {
        wxMessageBox(wxT("No records are saved in this ADT!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
    }
  }

void ProjectFrame::OnPostorderSplay(wxCommandEvent& event)
  {
    if (!splayList.isEmpty()) {
      // Clear the edit box
      MainEditBox->Clear();     

      string records = splayList.postorder();
        
      //Convert to a wxString
      wxString wxRecords(records.c_str(), wxConvUTF8);

      //display the words in the MainEditBox
      MainEditBox->AppendText(wxT("\t\t\t\t\nRecords in Splay Tree Postorder:\n\n"));
      MainEditBox->AppendText(wxRecords);
    } else {
        wxMessageBox(wxT("No records are saved in this ADT!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
    }
  }

//=============================================================================
//============== END Definition for the Splay Tree Functions ==================
//=============================================================================

//=============================================================================
//============== Definition for the BTree Functions ===========================
//=============================================================================

void ProjectFrame::OnCreateBTree(wxCommandEvent& event)
{
        long    num;
        string  first;
        string  last;
        string  maj;
        float   score;
 //       string  line;

        string record, heading, dummyString;
        
    // Clear the edit box
    MainEditBox->Clear();

    //Empty the BST
    btreeList.purgeBTree();   // Write that function!
      
    //open the file
    ifstream infile(CurrentDocPath.mb_str(), ios::in);

    // Check to see if a valid file was opened.
    if (!infile)
      {
          MainEditBox->AppendText(wxT("\n\n\t\t\t\tNo Data file opened as yet...\n\n"));
        return;
      }
    
        // Read off the heading line
        infile >> dummyString >> dummyString >> dummyString >> dummyString >> dummyString;
  
        // Create a label for the displayed records
        heading = "IDNo \tName \t\t\tSurname \t\tMajor \t\tGPA\n";
        heading.append("=====================================================\n");

        
        // Convert "heading" into a wxString and display it in the MainEditBox
        wxString headingLine(heading.c_str(), wxConvUTF8);
        MainEditBox->AppendText(headingLine);

        while (!infile.eof())
            {
                infile >> num >> first >> last >> maj >> score;
 
                if (!infile.eof())
                    {
                        if (maj == "IT") {
                            record = makeRecord(num, first, last, maj, score);
                                       
                            record.append("\t\t...Inserted\n");
                            btreeList.insert(num, first, last, maj, score);
                                                        
                            wxString wxRecord(record.c_str(), wxConvUTF8);
                            MainEditBox->AppendText(wxRecord);
                                
                            //Reset record variable
                            record = "";
                        }
                    }
            }  // End while
}

void ProjectFrame::OnAddBTree(wxCommandEvent& event)
{
    if (TRUE) {
        long    num;
        string  first;
        string  last;
        string  maj;
        float   score;

        string holder = "";
        string record;

        DataDialog* dlg = new DataDialog( wxT("COMP2611 - Data Entry"), 
               wxPoint(200,200), wxSize(600,300) );

        if (dlg->ShowModal() == wxID_OK) {
            num = dlg->IdNoEditBox->GetValue();
            first = dlg->NameEditBox->GetValue();
            last = dlg->SurnameEditBox->GetValue();
            maj = dlg->majorCombo->GetValue();
            score = dlg->GPAEditBox->GetValue();

            if (maj == "COMP") {
                queueList.enqueue(num, first, last, maj, score);
                priorityQueueList.enqueue(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                avlList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "IT") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                stackList.push(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                rbList.insert(num, first, last, maj, score);
                btreeList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "MATH") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                dequeList.enqueueHead(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                splayList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "ELET") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                dequeList.enqueueTail(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "PHYS") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            }

            OnDisplayBTree(event);
        }
        else {
          dlg->Close();
        }
        dlg->Destroy();
    }
}

void ProjectFrame::OnFindDataBTree(wxCommandEvent& event)
{
    if (TRUE) {
        long    num;
        string  first;
        string  last;
        string  maj;
        float   score;

        string holder = "";
        string record;

        wxTextEntryDialog dlg(this, "Enter ID of record to be searched");

        if (dlg.ShowModal() == wxID_OK) {
            wxMessageBox(holder = dlg.GetValue());
            stringstream record(holder);
            record >> num;

            MainEditBox->Clear();

            string found = btreeList.search(num);
            //Convert to a wxString
            wxString wxRecords(found.c_str(), wxConvUTF8);

            //display the words in the MainEditBox
            MainEditBox->AppendText(wxT("\t\t\t\t\nRecords at the same node in BTree as searched ID:\n\n"));
            MainEditBox->AppendText(wxRecords);
          }       
    }
}

void ProjectFrame::OnDisplayBTree(wxCommandEvent& event)
  {
    if (!btreeList.isEmpty()) {
      // Clear the edit box
      MainEditBox->Clear();     

      string records = btreeList.traverse();
        
      //Convert to a wxString
      wxString wxRecords(records.c_str(), wxConvUTF8);

      //display the words in the MainEditBox
      MainEditBox->AppendText(wxT("\t\t\t\t\nRecords in BTree:\n\n"));
      MainEditBox->AppendText(wxRecords);
    } else {
        wxMessageBox(wxT("No records are saved in this ADT!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
    }
  }

//=============================================================================
//============== END Definition for the BTree Functions =======================
//=============================================================================

//=============================================================================
//============== Definition for the Set Tree Functions ========================
//=============================================================================

void ProjectFrame::OnCreateSets(wxCommandEvent& event)
{
        long    num;
        string  first;
        string  last;
        string  maj;
        float   score;
 //       string  line;

        string record, heading, dummyString;
        
    // Clear the edit box
    MainEditBox->Clear();

    //Empty the Queue
    setAList.purgeSet();   // Write that function!
    setBList.purgeSet();
      
    //open the file
    ifstream infile(CurrentDocPath.mb_str(), ios::in);

    // Check to see if a valid file was opened.
    if (!infile)
      {
          MainEditBox->AppendText(wxT("\n\n\t\t\t\tNo Data file opened as yet...\n\n"));
        return;
      }
    
        // Read off the heading line
        infile >> dummyString >> dummyString >> dummyString >> dummyString >> dummyString;
  
        // Create a label for the displayed records
        heading = "IDNo \tName \t\t\tSurname \t\tMajor \t\tGPA\n";
        heading.append("=====================================================\n");

        
        // Convert "heading" into a wxString and display it in the MainEditBox
        wxString headingLine(heading.c_str(), wxConvUTF8);
        MainEditBox->AppendText(headingLine);

        while (!infile.eof())
            {
                infile >> num >> first >> last >> maj >> score;
 
                if (!infile.eof())
                    {
                        if (score < 3.0) {
                            record = makeRecord(num, first, last, maj, score);
                                    
                            record.append("\t\t...Added to Set A\n");
                            setAList.addTail(num, first, last, maj, score);
                                                    
                            wxString wxRecord(record.c_str(), wxConvUTF8);
                            MainEditBox->AppendText(wxRecord);
                            
                            //Reset record variable
                            record = "";
                        }
                        else if (score >= 3.0) {
                            record = makeRecord(num, first, last, maj, score);
                                    
                            record.append("\t\t...Added to Set B\n");
                            setBList.addTail(num, first, last, maj, score);
                                                    
                            wxString wxRecord(record.c_str(), wxConvUTF8);
                            MainEditBox->AppendText(wxRecord);
                            
                            //Reset record variable
                            record = "";
                        }
                    }
            }  // End while
}

void ProjectFrame::OnAddSet(wxCommandEvent& event)
{
    if (TRUE) {
        long    num;
        string  first;
        string  last;
        string  maj;
        float   score;

        string holder = "";
        string record;

        DataDialog* dlg = new DataDialog( wxT("COMP2611 - Data Entry"), 
               wxPoint(200,200), wxSize(600,300) );

        if (dlg->ShowModal() == wxID_OK) {
            num = dlg->IdNoEditBox->GetValue();
            first = dlg->NameEditBox->GetValue();
            last = dlg->SurnameEditBox->GetValue();
            maj = dlg->majorCombo->GetValue();
            score = dlg->GPAEditBox->GetValue();

            if (maj == "COMP") {
                queueList.enqueue(num, first, last, maj, score);
                priorityQueueList.enqueue(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                avlList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "IT") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                stackList.push(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                rbList.insert(num, first, last, maj, score);
                btreeList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "MATH") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                dequeList.enqueueHead(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                splayList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "ELET") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                dequeList.enqueueTail(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            } else if (maj == "PHYS") {
                priorityQueueList.enqueue(num, first, last, maj, score);
                bstList.insert(num, first, last, maj, score);
                heapList.insert(num, first, last, maj, score);
                if (score < 3.0)
                    setAList.addTail(num, first, last, maj, score);
                if (score >= 3.0)
                    setBList.addTail(num, first, last, maj, score);
            }

            OnInorderSplay(event);
        }
        else {
          dlg->Close();
        }
        dlg->Destroy();
    }
}

void ProjectFrame::OnDisplaySetA(wxCommandEvent& event)
  {
    if (!setAList.isEmpty()) {
      // Clear the edit box
      MainEditBox->Clear();     

      string records = setAList.showNodes();
        
      //Convert to a wxString
      wxString wxRecords(records.c_str(), wxConvUTF8);

      //display the words in the MainEditBox
      MainEditBox->AppendText(wxT("\t\t\t\t\nRecords in Set A:\n\n"));
      MainEditBox->AppendText(wxRecords);
    } else {
        wxMessageBox(wxT("No records are saved in this ADT!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
    }
  }

void ProjectFrame::OnDisplaySetB(wxCommandEvent& event)
  {
    if (!setBList.isEmpty()) {
      // Clear the edit box
      MainEditBox->Clear();     

      string records = setBList.showNodes();
        
      //Convert to a wxString
      wxString wxRecords(records.c_str(), wxConvUTF8);

      //display the words in the MainEditBox
      MainEditBox->AppendText(wxT("\t\t\t\t\nRecords in Set B:\n\n"));
      MainEditBox->AppendText(wxRecords);
    } else {
        wxMessageBox(wxT("No records are saved in this ADT!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
    }
  }

void ProjectFrame::OnDisplayIntersection(wxCommandEvent& event)
  {
    if (!setAList.isEmpty()) {
      // Clear the edit box
      MainEditBox->Clear();     

      string records = setAList.showIntersection(setBList);
        
      //Convert to a wxString
      wxString wxRecords(records.c_str(), wxConvUTF8);

      //display the words in the MainEditBox
      MainEditBox->AppendText(wxT("\t\t\t\t\nIntersection of Set A and Set B:\n\n"));
      MainEditBox->AppendText(wxRecords);
    } else {
        wxMessageBox(wxT("No records are saved in this ADT!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
    }
  }

void ProjectFrame::OnDisplayUnion(wxCommandEvent& event)
  {
    if (!setAList.isEmpty() && !setBList.isEmpty()) {
      // Clear the edit box
      MainEditBox->Clear();     

      string records = setAList.showUnion(setBList);
        
      //Convert to a wxString
      wxString wxRecords(records.c_str(), wxConvUTF8);

      //display the words in the MainEditBox
      MainEditBox->AppendText(wxT("\t\t\t\t\nUnion of Set A and Set B:\n\n"));
      MainEditBox->AppendText(wxRecords);
    } else {
        wxMessageBox(wxT("No records are saved in this ADT!"), wxT("Exit"), wxOK | wxICON_INFORMATION, this);
    }
  }

void ProjectFrame::OnDeleteSetA(wxCommandEvent& event)
{
    if (TRUE) {
        long    num;
        string  first;
        string  last;
        string  maj;
        float   score;

        string holder = "";
        string record;

        wxTextEntryDialog dlg(this, "Enter Data of the Record to be removed");

        if (dlg.ShowModal() == wxID_OK) {
            wxMessageBox(holder = dlg.GetValue());
            stringstream record(holder);
            record >> num;

            queueList.removeTarget(num);
            dequeList.removeTarget(num);
            priorityQueueList.removeTarget(num);
            stackList.removeTarget(num);
            bstList.remove(num);
            avlList.remove(num);
            heapList.remove(num);
            rbList.remove(num);
            splayList.remove(num);
            setAList.delNode(num);
            setBList.delNode(num);
        }
    }
}

void ProjectFrame::OnDeleteSetB(wxCommandEvent& event)
{
    if (TRUE) {
        long    num;
        string  first;
        string  last;
        string  maj;
        float   score;

        string holder = "";
        string record;

        wxTextEntryDialog dlg(this, "Enter Data of the Record to be removed");

        if (dlg.ShowModal() == wxID_OK) {
            wxMessageBox(holder = dlg.GetValue());
            stringstream record(holder);
            record >> num;

            queueList.removeTarget(num);
            dequeList.removeTarget(num);
            priorityQueueList.removeTarget(num);
            stackList.removeTarget(num);
            bstList.remove(num);
            avlList.remove(num);
            heapList.remove(num);
            rbList.remove(num);
            splayList.remove(num);
            setAList.delNode(num);
            setBList.delNode(num);
        }
    }
}

//=============================================================================
//============== END Definition for the Set Tree Functions ====================
//=============================================================================