#ifndef DATADIALOG_H
#define DATADIALOG_H


   
  
//Define a dialog box class with 5 data fields (Attributes) - inheriting from wxDialog, of course 
class DataDialog: public wxDialog
  {
	 public:
		DataDialog(const wxString& title, const wxPoint& pos, const wxSize& size);
			  
		wxSpinCtrl* IdNoEditBox;
		wxTextCtrl* NameEditBox;
		wxTextCtrl* SurnameEditBox;
		wxSpinCtrlDouble* GPAEditBox;
		
		wxComboBox* majorCombo;		//For a drop-down list
  };

  
  
DataDialog::DataDialog ( const wxString& title, const wxPoint& pos, 
		   const wxSize& size): wxDialog((wxDialog *)NULL, -1, title, pos, size)
  {
		//Set up the panel
  		wxPanel    *panel = new wxPanel(this, -1);
 
		//Define and position the StaticTexts - ie the labels
		wxStaticText *idNoLabel = new wxStaticText(panel, wxID_ANY,
						  wxT("ID Number"), wxPoint(15, 25) );
  
		wxStaticText *nameLabel 	= new wxStaticText(panel, wxID_ANY,
						  wxT("Name"), wxPoint(15, 60) );
  
		wxStaticText *surnameLabel 	= new wxStaticText(panel, wxID_ANY,
						  wxT("Surname"), wxPoint(15, 95) );

		wxStaticText *gpaLabel = new wxStaticText(panel, wxID_ANY,
						wxT("GPA"), wxPoint(15, 130) );
  
 		wxStaticText *majorLabel	= new wxStaticText(panel, wxID_ANY,
						  wxT("Major"), wxPoint(325, 25) );
  
  
		//Define and position the Data Boxes
		IdNoEditBox = new wxSpinCtrl(panel, -1, wxT("40000000"), wxPoint(100, 25), 
					      wxSize(150, -1), wxSP_ARROW_KEYS, 40000000,
					      99999999, 1);
		
		NameEditBox 	=  new wxTextCtrl ( panel, wxID_ANY, wxT("Name"), 
						       wxPoint(100, 60), wxSize(170, -1) );
		SurnameEditBox 	=  new wxTextCtrl ( panel, wxID_ANY, wxT("Surname"), 
						       wxPoint(100, 95), wxSize(170, -1) );

		GPAEditBox = new wxSpinCtrlDouble(panel, -1, wxT("0.0"), wxPoint(100, 130), 
					      wxSize(150, -1), wxSP_ARROW_KEYS, 0.0,
					      4.0, 0.0, 0.01);
		
		//Define and position a drop-down list for Season
		wxArrayString majors;
		majors.Add(wxT("COMP"));
		majors.Add(wxT("MATH"));
		majors.Add(wxT("IT"));
		majors.Add(wxT("PHYS"));
		majors.Add(wxT("ELET"));
		majorCombo = new wxComboBox(panel, -1, wxT("COMP"), wxPoint(320, 45), 
					       wxSize(120, -1), majors, wxCB_READONLY);

		
        
		// Define and position the Response buttons (OK and CANCEL)
		
		// The OK button
		wxButton* ok = new wxButton(panel, wxID_OK, wxT("&OK"), 
					       wxPoint(200, 230), wxDefaultSize, 0);

		// The CANCEL button
		wxButton* cancel = new wxButton ( panel, wxID_CANCEL, wxT("&CANCEL"), 
						       wxPoint(330, 230), wxDefaultSize, 0 );
		
  		// Centre the dialog on the parent's window
		Centre();

        //  Maximize(); // Maximize the window
		
  }

#endif