#ifndef QUEUE_H
#define QUEUE_H

#include "Node.h"
#include <string>

using namespace std;

class Queue {
private:
	Node* frontPtr;
public:
	Queue(void);
	void enqueue(long, string, string, string, float);
	string dequeue(void);
	bool find(long);
	string showNodes(void);
	bool isEmpty(void);
	int number(void);
	void purgeQueue(void);

	string showHead(void);
	string showTail(void);

	void removeTarget(long id);
};

Queue::Queue(void) {
	frontPtr = new Node();
}

void Queue::enqueue(long id, string n, string sn, string m, float g) {
	Node* iPtr = frontPtr;
	while (iPtr->getNext() != nullptr) {
		iPtr = iPtr->getNext();
	}

	//cout << id << n << sn << m << g << endl;

	Node* newPtr = new Node(id, n, sn, m, g);
	newPtr->setNext(iPtr->getNext());
	iPtr->setNext(newPtr);
}

string Queue::dequeue(void) {
	string nodeData = "";

	Node* iPtr = frontPtr->getNext();
	nodeData.append(to_string(iPtr->getIdNo()) + "\t" + iPtr->getName() + "\t" + iPtr->getSurname() + "\t" + iPtr->getMajor() + 
					"\t" + to_string(iPtr->getGpa()) + "\n");

	frontPtr->setNext(iPtr->getNext());
	delete iPtr;

	return nodeData;
}

bool Queue::find(long id) {
	Node* iPtr = frontPtr;
	while (iPtr != nullptr) {
		if (iPtr->getIdNo() == id) 
			return true;

		iPtr = iPtr->getNext();
	}
	return false;
}

string Queue::showNodes(void) {
	Node* iPtr = frontPtr->getNext();
	string nodeData = "";

	while (iPtr != nullptr) {
		nodeData.append(to_string(iPtr->getIdNo()) + "\t" + iPtr->getName() + "\t" + iPtr->getSurname() + "\t" + iPtr->getMajor() + 
					"\t" + to_string(iPtr->getGpa()) + "\n");

		iPtr = iPtr->getNext();
	}

	return nodeData;
}

bool Queue::isEmpty(void) {
	if (frontPtr->getNext() == nullptr)
		return true;

	return false;
}

int Queue::number(void) {
	int i = 0;

	Node* iPtr = frontPtr;
	while (iPtr != nullptr) {
		i++;
		iPtr = iPtr->getNext();
	}

	return i;
}

void Queue::purgeQueue(void) {
	frontPtr = new Node();
}

string Queue::showHead(void) {
	string nodeData = "";

	if (!isEmpty()) {
		Node* iPtr = frontPtr->getNext();

		nodeData.append(to_string(iPtr->getIdNo()) + "\t" + iPtr->getName() + "\t" + iPtr->getSurname() + "\t" + iPtr->getMajor() + 
					"\t" + to_string(iPtr->getGpa()) + "\n");
	}

	return nodeData;
}

string Queue::showTail(void) {
	string nodeData = "";
	
	if (!isEmpty()) {
		Node* iPtr = frontPtr->getNext();

		while (iPtr->getNext() != nullptr) {
			iPtr = iPtr->getNext();
		}

		nodeData.append(to_string(iPtr->getIdNo()) + "\t" + iPtr->getName() + "\t" + iPtr->getSurname() + "\t" + iPtr->getMajor() + 
					"\t" + to_string(iPtr->getGpa()) + "\n");
	}

	return nodeData;
}

void Queue::removeTarget(long id) {
	Node* iPtr = frontPtr;

	while (iPtr != nullptr && iPtr->getNext() != nullptr) {
		if (iPtr->getNext()->getIdNo() == id) {
			Node* tempPtr = iPtr->getNext();
			iPtr->setNext(tempPtr->getNext());
			delete tempPtr;
		}

		iPtr = iPtr->getNext();
	}
}

#endif
