#ifndef RB_H
#define RB_H
//#include "Node.h"


class RBNode
    {
        private:
            long data;
            string color;
	    
	        long  idNo;
            string  name;
            string  surname;
            string  major;
	    	float  gpa;
    
        public:
            RBNode* left;
            RBNode* right;
            RBNode* parent;

            RBNode(){right = left = parent= NULL; color = "RED";}
            RBNode(long num, string first, string last, string maj, float score);
    
            // Mutator functions
            void setData(long val){idNo = val;}
            void setColor(string col){color = col;}

            // Accessor functions
            string getRecord();
            long getData(){return idNo;}
            RBNode* getLeft(){ return left; }
	    	RBNode* getRight()      { return right; }
	        long getID(){ return idNo; }
            string getColor(){return color;}

            long getIdNo() { return idNo; }
            string getName() { return name; }
            string getSurname() { return surname; }
            string getMajor() { return major; }
            float getGpa() { return gpa; }
     };

RBNode::RBNode(long num, string first, string last, string maj, float score)
    {
        idNo        = num;
        name        = first;
        surname	    = last;
        major       = maj;
        gpa  = score;
        left        = NULL;
        right       = NULL;
        color = "RED";
    }

string RBNode::getRecord()
    {
      string record = to_string(idNo);
	  record.append(" \t");
	  record.append(name);
	  record.append(" \t\t");
	  record.append(surname);
	  record.append(" \t\t\t");
	  record.append(major);
	  record.append(" \t\t");
	  record.append(to_string(gpa));
	  record.append(" \n");
        
        return record;
    }

class RB
    {
        private:
            RBNode* root;
    
            // Utility functions
            void    rotateLeft( RBNode* );
            void    rotateRight(RBNode* );
            void    fixUp(RBNode* );
            string  inorderHelper(RBNode* );
            string  preorderHelper(RBNode* );
            string  postorderHelper(RBNode* );
            void    deleteNode(RBNode* );
            RBNode* findNode(long );
            RBNode* getMinimum(RBNode *);
            RBNode* getSuccessor(RBNode *);
    
        public:
            // Constructor function
            RB(){ root = NULL;}
    
            // Mutator functions
            void insert(long num, string first, string last, string maj, float score);
            void remove(long);

            bool isEmpty();
    
            // Accessor functions
            RBNode* getRoot(){ return root;}
            string inorder();
            string preorder();
            string postorder();
            string showLevels();
            void purgeRB() { root = nullptr; }
     };





void RB::rotateLeft( RBNode* ptr )
	{
        RBNode* rightChild = ptr->right;
        ptr->right = rightChild->left;
        
        if (rightChild->left != NULL)
            rightChild->left->parent = ptr;
        
        rightChild->parent = ptr->parent;
        
        if (ptr == root)
            root = rightChild;
        else
            {
                if( ptr == ptr->parent->left)
                    ptr->parent->left = rightChild;
                 else
                    ptr->parent->right = rightChild;
            }
    
        rightChild->left = ptr;
        ptr->parent = rightChild;
 	}




void RB::rotateRight(RBNode* ptr)
	{
        RBNode* leftChild = ptr->left;
        ptr->left = leftChild->right;
        
        if (leftChild->right != NULL)
            leftChild->right->parent = ptr;
        
        leftChild->parent = ptr->parent;
        
        if (ptr == root)
            root = leftChild;
        else
            {
                if( ptr == ptr->parent->right)
                    ptr->parent->right = leftChild;
                 else
                    ptr->parent->left = leftChild;
            }
    
        leftChild->right = ptr;
        ptr->parent = leftChild;
    
    }





RBNode* RB::findNode(long  data)
	{
		RBNode* ptr = root;
		
		while ( ptr != NULL )
            {
                if  (ptr->getData() == data)        // Found it!
                    {
                        return ptr;
                    }
            
                if (data < ptr->getData() )
                    ptr = ptr->left;
                else 
                    ptr = ptr->right;
            }
    
        return NULL;            // Tree is empty OR did not find it
	} 





void RB::insert(long num, string first, string last, string maj, float score)
	{
    
        RBNode* newnode = new RBNode(num, first, last, maj, score);      // Create the new node
        
        if (root == NULL)                   // Tree is empty - newnode is first node
            {
                newnode->setColor("BLACK");
                root = newnode;
                return;
            }
        else                                // Tree is NOT empty
            {
                RBNode* ancestor = NULL;
                RBNode* current = root;
                while (current != NULL)
                    {
                        ancestor = current;
                        
                        if ( newnode->getData() < current->getData() )
                            current = current->left;
                        else
                            current = current->right;
                    }
            
                newnode->parent = ancestor;
                
                if ( newnode->getData() < ancestor->getData() )
                    ancestor->left = newnode;
                else
                    ancestor->right= newnode;
            
                fixUp(newnode);
            }
	}




void RB::fixUp( RBNode *ptr )
    {
        RBNode *ptrsUncle = NULL;
    
        while ( ptr != root && ptr->parent->getColor() == "RED" )
            {
                if ( ptr->parent == ptr->parent->parent->left )
                    {       // ptr's parent is a LEFT child
                        ptrsUncle = ptr->parent->parent->right;
                        
                        if (  ptrsUncle != NULL && ptrsUncle->getColor() == "RED" )
                            {
                                ptr->parent->setColor("BLACK");
                                ptrsUncle->setColor("BLACK");
                                ptr->parent->parent->setColor("RED");
                                ptr = ptr->parent->parent;
                            }
                        else
                            {
                                if ( ptr == ptr->parent->right )
                                    {
                                        ptr = ptr->parent;
                                        rotateLeft( ptr );
                                    }
                                
                                ptr->parent->setColor("BLACK");
                                ptr->parent->parent->setColor("RED");
                                rotateRight( ptr->parent->parent );
                            }
                    }
                else            // ptr's parent is a RIGHT child
                    {
                        ptrsUncle = ptr->parent->parent->left;
                        
                        if ( ptrsUncle != NULL && ptrsUncle->getColor() == "RED" )
                            {
                                ptr->parent->setColor("BLACK");
                                ptrsUncle->setColor("BLACK");
                                ptr->parent->parent->setColor("RED");
                                ptr = ptr->parent->parent;
                            }
                        else
                            {
                                if ( ptr == ptr->parent->left )
                                    {
                                        ptr = ptr->parent;
                                        rotateRight( ptr );
                                    }
                                ptr->parent->setColor("BLACK");
                                ptr->parent->parent->setColor("RED");
                                rotateLeft( ptr->parent->parent );
                            }
                    }
            }
        
        root->setColor("BLACK");
        
        ptrsUncle = NULL;
    }




void RB::remove(long val)
    {
        RBNode* markedNode = findNode(val);
        deleteNode(markedNode);
    }




void RB::deleteNode(RBNode* node2Zap)
    {
        if (node2Zap == NULL)    //If node being deleted is NULL then do nothing.
            return;
        else
            {
                RBNode *successor, *successorChild;
                successor = node2Zap;
            
                if (node2Zap->left == NULL || node2Zap->right == NULL)
                        //If either of the children is NULL
                    successor = node2Zap;
                else                    // node2Zap has 2 children
                    successor = getSuccessor(node2Zap);
            
                if (successor->left == NULL)
                    successorChild = successor->right;
                else
                    successorChild = successor->right;
                
                if (successorChild != NULL)
                    successorChild->parent = successor->parent;
                
                if (successor->parent == NULL)
                    root = successorChild;
                else if (successor == successor->parent->left)
                        successor->parent->left = successorChild;
                    else
                        successor->parent->right = successorChild;
                
                if (successor != node2Zap)
                    node2Zap->setData(successor->getData());

                // Finally... If color is black call fixup otherwise no fixup required
                if (successor->getColor() == "BLACK" && successorChild != NULL)
                    fixUp(successorChild);
            }
    }




RBNode* RB::getSuccessor(RBNode* ptr)
    {
        if (ptr->right == NULL)
            return ptr->left;
        else
            return(getMinimum(ptr->right));
    }




RBNode* RB::getMinimum(RBNode *ptr)
    {
        while(ptr->left != NULL)
            ptr = ptr->left;
    
        return ptr;
    }




string RB::inorderHelper(RBNode* ptr)
	{
	  string nodeData = "";
    string leftChildId = "Null";
    string rightChildId = "Null";

    if (ptr != nullptr) {
        if (ptr->left != nullptr) {
            leftChildId = to_string(ptr->left->getIdNo());
        }
        if (ptr->right != nullptr) {
            rightChildId = to_string(ptr->right->getIdNo());
        }

        nodeData.append(inorderHelper(ptr->left));

        nodeData.append(to_string(ptr->getIdNo()) + "\t" + ptr->getName() + "\t" + ptr->getSurname() + "\t" + ptr->getMajor() + 
                    "\t" + to_string(ptr->getGpa()) + "\t\t" + "[" + leftChildId + ", " + rightChildId + "] [" + ptr->getColor() +
                     "]" + "\n");

        nodeData.append(inorderHelper(ptr->right));
    }

    return nodeData;
	}




string RB::preorderHelper(RBNode* ptr)
	{
	string nodeData = "";
    string leftChildId = "Null";
    string rightChildId = "Null";

    if (ptr != nullptr) {
        if (ptr->left != nullptr) {
            leftChildId = to_string(ptr->left->getIdNo());
        }
        if (ptr->right != nullptr) {
            rightChildId = to_string(ptr->right->getIdNo());
        }
        
        nodeData.append(to_string(ptr->getIdNo()) + "\t" + ptr->getName() + "\t" + ptr->getSurname() + "\t" + ptr->getMajor() + 
                    "\t" + to_string(ptr->getGpa()) + "\t\t" + "[" + leftChildId + ", " + rightChildId + "] [" + ptr->getColor()
                     + "]" + "\n");

        nodeData.append(preorderHelper(ptr->left));
        nodeData.append(preorderHelper(ptr->right));
    }

    return nodeData;
	}




string RB::postorderHelper(RBNode* ptr)
	{
       string nodeData = "";
    string leftChildId = "Null";
    string rightChildId = "Null";

    if (ptr != nullptr) {
        if (ptr->left != nullptr) {
            leftChildId = to_string(ptr->left->getIdNo());
        }
        if (ptr->right != nullptr) {
            rightChildId = to_string(ptr->right->getIdNo());
        }
        

        nodeData.append(postorderHelper(ptr->left));
        nodeData.append(postorderHelper(ptr->right));

        nodeData.append(to_string(ptr->getIdNo()) + "\t" + ptr->getName() + "\t" + ptr->getSurname() + "\t" + ptr->getMajor() + 
                    "\t" + to_string(ptr->getGpa()) + "\t\t" + "[" + leftChildId + ", " + rightChildId + "] [" + ptr->getColor()
                     + "]" + "\n");
    }

    return nodeData;
	}




string RB::inorder()
	{
		return inorderHelper(root);
	}




string RB::preorder()
    {
        return preorderHelper(root);
    }




string RB::postorder()
    {
        return postorderHelper(root);
    }




string RB::showLevels()
    {
        vector <RBNode*> listA;
        vector <RBNode*> listB;
    
        string str = "";
        
        listA.resize(0);
        listB.resize(0);
        
        if (root == NULL)
            {
                str.append("Tree is Empty...\n");
                return str;
            }
        else
            listA.push_back(root);

    
        while (!listA.empty())
            {
                //Display the contents of listA
                for (int x = 0; x < listA.size(); x++)
                    {
                        str.append( to_string(listA[x]->getData()) );
                        str.append("{");
                        str.append( listA[x]->getColor());
                        str.append("} \t");
                     }
                str.append("\n");
            
                //Transfer contents of listA to listB
                listB = listA;
            
                // Now purge listA and copy listB's children into listA
                listA.resize(0);

                for (int x = 0; x < listB.size(); x++)
                    {
                        if (listB[x]->left != NULL)
                            listA.push_back(listB[x]->left);
                        if (listB[x]->right != NULL)
                            listA.push_back(listB[x]->right);
                    }
            }
        return str;
    }

bool RB::isEmpty() {
    if (root == nullptr)
        return true;
    return false;
}



#endif /* defined(RB_H) */
