#ifndef PriorityQueue_h
#define PriorityQueue_h

#include "Node.h"

using namespace std;

class PriorityQueue {
private:
	Node* frontPtr;
public:
	PriorityQueue(void);
	void enqueue(long, string, string, string, float);
	string dequeue(void);
	bool find(long);
	string showNodes(void);
	bool isEmpty(void);
	int number(void);

	string showHead(void);
	string showTail(void);
	void purgePriorityQueue(void);

	void removeTarget(long id);
};

PriorityQueue::PriorityQueue(void) {
	frontPtr = new Node();
}

void PriorityQueue::enqueue(long id, string n, string sn, string m, float g) {
	Node* iPtr = frontPtr;
	Node* newPtr = new Node(id, n, sn, m, g);
	
	while (iPtr->getNext() != nullptr && newPtr->getIdNo() >= iPtr->getNext()->getIdNo()) {
		iPtr = iPtr->getNext();
	}

	newPtr->setNext(iPtr->getNext());
	iPtr->setNext(newPtr);
}

string PriorityQueue::dequeue(void) {
	string nodeData = "";

	Node* iPtr = frontPtr->getNext();
	nodeData.append(to_string(iPtr->getIdNo()) + "\t" + iPtr->getName() + "\t" + iPtr->getSurname() + "\t" + iPtr->getMajor() + 
					"\t" + to_string(iPtr->getGpa()) + "\n");

	frontPtr->setNext(iPtr->getNext());
	delete iPtr;

	return nodeData;
}

bool PriorityQueue::find(long id) {
	Node* iPtr = frontPtr;
	while (iPtr != nullptr) {
		if (iPtr->getIdNo() == id) 
			return true;

		iPtr = iPtr->getNext();
	}
	return false;
}

string PriorityQueue::showNodes(void) {
	Node* iPtr = frontPtr->getNext();
	string nodeData = "";

	while (iPtr != nullptr) {
		nodeData.append(to_string(iPtr->getIdNo()) + "\t" + iPtr->getName() + "\t" + iPtr->getSurname() + "\t" + iPtr->getMajor() + 
					"\t" + to_string(iPtr->getGpa()) + "\n");

		iPtr = iPtr->getNext();
	}

	return nodeData;
}

bool PriorityQueue::isEmpty(void) {
	if (frontPtr->getNext() == nullptr)
		return true;

	return false;
}

int PriorityQueue::number(void) {
	int i = 0;

	Node* iPtr = frontPtr;
	while (iPtr != nullptr) {
		i++;
		iPtr = iPtr->getNext();
	}

	return i;
}

string PriorityQueue::showHead(void) {
	string nodeData = "";

	if (!isEmpty()) {
		Node* iPtr = frontPtr->getNext();

		nodeData.append(to_string(iPtr->getIdNo()) + "\t" + iPtr->getName() + "\t" + iPtr->getSurname() + "\t" + iPtr->getMajor() + 
					"\t" + to_string(iPtr->getGpa()) + "\n");
	}

	return nodeData;
}

string PriorityQueue::showTail(void) {
	string nodeData = "";
	
	if (!isEmpty()) {
		Node* iPtr = frontPtr->getNext();

		while (iPtr->getNext() != nullptr) {
			iPtr = iPtr->getNext();
		}

		nodeData.append(to_string(iPtr->getIdNo()) + "\t" + iPtr->getName() + "\t" + iPtr->getSurname() + "\t" + iPtr->getMajor() + 
					"\t" + to_string(iPtr->getGpa()) + "\n");
	}

	return nodeData;
}

void PriorityQueue::purgePriorityQueue(void) {
	frontPtr = new Node();
}

void PriorityQueue::removeTarget(long id) {
	Node* iPtr = frontPtr;

	while (iPtr != nullptr && iPtr->getNext() != nullptr) {
		if (iPtr->getNext()->getIdNo() == id) {
			Node* tempPtr = iPtr->getNext();
			iPtr->setNext(tempPtr->getNext());
			delete tempPtr;
		}

		iPtr = iPtr->getNext();
	}
}

#endif
