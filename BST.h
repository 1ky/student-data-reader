#ifndef BST_H
#define BST_H

#include <string.h>

using namespace std;

class BSTNode {
private:
	long idNo;
	string name;
	string surname;
	string major;
	float gpa;

public:
	BSTNode* left;
	BSTNode* right;

	BSTNode();
	BSTNode(long id, string n, string sn, string m, float g);

	void setIdNo(long id) { idNo = id; }
	void setName(string n) { name = n; }
	void setSurname(string sn) { surname = sn; }
	void setMajor(string m) { major = m; }
	void setGpa(float g) { gpa = g; }
	void setLeft(BSTNode* ptr) { left = ptr; }
	void setRight(BSTNode* ptr) { right = ptr; }

	long getIdNo() { return idNo; }
	string getName() { return name; }
	string getSurname() { return surname; }
	string getMajor() { return major; }
	float getGpa() { return gpa; }
	BSTNode* getLeft() { return left; }
	BSTNode* getRight() { return right; }
};

class BST {
private:
	BSTNode* root;
	BSTNode* insertHelper(BSTNode* ptr, long id, string n, string sn, string m, float gpa);
	BSTNode* deleteNode(BSTNode*, long id);
	string inorderHelper(BSTNode* ptr);
	string preorderHelper(BSTNode* ptr);
	string postorderHelper(BSTNode* ptr);

public:
	BST() { root = nullptr; }

	bool isEmpty();
	void purgeBST() { root = nullptr; }

	void insert(long id, string n, string sn, string m, float gpa) { root = insertHelper(root, id, n, sn, m, gpa); }
	void remove(long id) { root = deleteNode(root, id); }

	string inorder() { return inorderHelper(root); }
	string preorder() { return preorderHelper(root); }
	string postorder() { return postorderHelper(root); }
};

BSTNode::BSTNode() {
	string n = "";

	setIdNo(0);
	setName(n);
	setSurname(n);
	setMajor(n);
	setGpa(0);
	setLeft(nullptr);
	setRight(nullptr);
}

BSTNode::BSTNode(long id, string n, string sn, string m, float g) {
	setIdNo(id);
	setName(n);
	setSurname(sn);
	setMajor(m);
	setGpa(g);
	setLeft(nullptr);
	setRight(nullptr);
}

bool BST::isEmpty() {
	if (root == nullptr)
		return true;
	return false;
}

BSTNode* BST::insertHelper(BSTNode* ptr, long id, string n, string sn, string m, float gpa) {
	if (ptr == nullptr)
		ptr = new BSTNode(id, n, sn, m, gpa);
	else {
		if (id >= ptr->getIdNo())
			ptr->right = insertHelper(ptr->right, id, n, sn, m, gpa);
		else
			ptr->left = insertHelper(ptr->left, id, n, sn, m, gpa);
	}
	return ptr;
}

BSTNode* BST::deleteNode(BSTNode* ptr, long id) {
	BSTNode* successor;

	if (ptr == nullptr)
		return nullptr;
	else {
		if (id > ptr->getIdNo())
			ptr->right = deleteNode(ptr->right, id);
		else if (id < ptr->getIdNo())
			ptr->left = deleteNode(ptr->left, id);
		else
			if (ptr->right != nullptr) {
				successor = ptr->right;
				while (successor->left != nullptr)
					successor = successor->left;

				ptr->setIdNo(successor->getIdNo());
				ptr->setName(successor->getName());
				ptr->setSurname(successor->getSurname());
				ptr->setMajor(successor->getMajor());
				ptr->setGpa(successor->getGpa());

				ptr->right = deleteNode(ptr->right, ptr->getIdNo());
			}
			else
				return ptr->left;
	}
	return ptr;
}

string BST::inorderHelper(BSTNode* ptr) {
	string nodeData = "";
	string leftChildId = "Null";
	string rightChildId = "Null";

	if (ptr != nullptr)	{
		if (ptr->left != nullptr) {
			leftChildId = to_string(ptr->left->getIdNo());
		}
		if (ptr->right != nullptr) {
			rightChildId = to_string(ptr->right->getIdNo());
		}

		nodeData.append(inorderHelper(ptr->left));

		nodeData.append(to_string(ptr->getIdNo()) + "\t" + ptr->getName() + "\t" + ptr->getSurname() + "\t" + ptr->getMajor() + 
					"\t" + to_string(ptr->getGpa()) + "\t\t" + "[" + leftChildId + ", " + rightChildId + "]" + "\n");

		nodeData.append(inorderHelper(ptr->right));
	}

	return nodeData;
}

string BST::preorderHelper(BSTNode* ptr) {
	string nodeData = "";
	string leftChildId = "Null";
	string rightChildId = "Null";

	if (ptr != nullptr)	{
		if (ptr->left != nullptr) {
			leftChildId = to_string(ptr->left->getIdNo());
		}
		if (ptr->right != nullptr) {
			rightChildId = to_string(ptr->right->getIdNo());
		}
		
		nodeData.append(to_string(ptr->getIdNo()) + "\t" + ptr->getName() + "\t" + ptr->getSurname() + "\t" + ptr->getMajor() + 
					"\t" + to_string(ptr->getGpa()) + "\t\t" + "[" + leftChildId + ", " + rightChildId + "]" + "\n");

		nodeData.append(preorderHelper(ptr->left));
		nodeData.append(preorderHelper(ptr->right));
	}

	return nodeData;
}

string BST::postorderHelper(BSTNode* ptr) {
	string nodeData = "";
	string leftChildId = "Null";
	string rightChildId = "Null";

	if (ptr != nullptr)	{
		if (ptr->left != nullptr) {
			leftChildId = to_string(ptr->left->getIdNo());
		}
		if (ptr->right != nullptr) {
			rightChildId = to_string(ptr->right->getIdNo());
		}
		

		nodeData.append(postorderHelper(ptr->left));
		nodeData.append(postorderHelper(ptr->right));

		nodeData.append(to_string(ptr->getIdNo()) + "\t" + ptr->getName() + "\t" + ptr->getSurname() + "\t" + ptr->getMajor() + 
					"\t" + to_string(ptr->getGpa()) + "\t\t" + "[" + leftChildId + ", " + rightChildId + "]" + "\n");
	}

	return nodeData;
}

#endif
