//
//  SplayTree.h
//  Splay
//
//  Created by Dr. John Charlery on 11/2/19.
//  Copyright (c) 2019 University of the West Indies. All rights reserved.
//

#ifndef SPLAYTREE_H
#define SPLAYTREE_H

#include <string.h>

using namespace std;

class SplayNode
    {
        private:
                long idNo;
                string name;
                string surname;
                string major;
                float gpa;
    
        public:
                SplayNode *left;  	// pointer to left subtree
                SplayNode *right;    // pointer to right subtree
    
                //Constructor
                SplayNode(long id, string n, string sn, string m, float g);
                SplayNode();
    
                //Accessors
                long getIdNo() { return idNo; }
                string getName() { return name; }
                string getSurname() { return surname; }
                string getMajor() { return major; }
                float getGpa() { return gpa; }
                SplayNode* getLeft()  const { return left;}
                SplayNode* getRight() const { return right;}
    
    
                //Mutators
                void setIdNo(long id) { idNo = id; }
                void setName(string n) { name = n; }
                void setSurname(string sn) { surname = sn; }
                void setMajor(string m) { major = m; }
                void setGpa(float g) { gpa = g; }
                void setLeft(SplayNode* ptr)  { left = ptr; }
                void setRight(SplayNode* ptr) { right = ptr; }
    };




class SplayTree
    {
        private:
                SplayNode *root;
    
                    // Utility functions
                SplayNode* RightRotate(SplayNode*);
                SplayNode* LeftRotate(SplayNode*);
                SplayNode* Splay(SplayNode* ptr, long id);
                SplayNode* InsertHelper(SplayNode*, long id, string n, string sn, string m, float gpa);
                string inorderHelper(SplayNode*);
                string preorderHelper(SplayNode*);
                string postorderHelper(SplayNode*);
                void insertSplayNodeHelper(SplayNode **, int );
                string displayNodesHelper( SplayNode * ) const;
                void deleteSplayNode( SplayNode* );
                SplayNode* findSplayNode( long );
                SplayNode* getSuccessor(SplayNode* );
                SplayNode* getParent(SplayNode* );
                SplayNode* getMinimum( SplayNode* );

    
            public:
                SplayTree(){ root = 0; };
                
                bool isEmpty();
                void purgeSplay() { root = nullptr; }

                SplayNode* getRoot(){return root;}
                void insert(long id, string n, string sn, string m, float gpa);
                string inorder(){ return inorderHelper(root);}
                string preorder(){ return preorderHelper(root);}
                string postorder(){ return postorderHelper(root);}
                void insertSplayNode( long );
                string displayNodes();
                void remove(long);
    };

SplayNode::SplayNode() {
    string n = "";

    setIdNo(0);
    setName(n);
    setSurname(n);
    setMajor(n);
    setGpa(0);
    setLeft(nullptr);
    setRight(nullptr);
}

SplayNode::SplayNode(long id, string n, string sn, string m, float g) {
    setIdNo(id);
    setName(n);
    setSurname(sn);
    setMajor(m);
    setGpa(g);
    setLeft(nullptr);
    setRight(nullptr);
}

bool SplayTree::isEmpty() {
    if (root == nullptr)
        return true;
    return false;
}

SplayNode* SplayTree::RightRotate(SplayNode* ptr)
{
    SplayNode* leftchild = ptr->left;
    ptr->left = leftchild->right;
    leftchild->right = ptr;
    return leftchild;
}




SplayNode* SplayTree::LeftRotate(SplayNode* ptr)
{
    SplayNode* rightchild = ptr->right;
    ptr->right = rightchild->left;
    rightchild->left = ptr;
    return rightchild;
}





SplayNode* SplayTree::Splay( SplayNode* root, long id)
    {
        if(root == NULL)
            return NULL;
        
        SplayNode* header = new SplayNode();
        
        SplayNode* LeftTreeMax = header;
        SplayNode* RightTreeMin = header;
        
        /* loop until root->left == NULL || root->right == NULL; then break!
         The zig/zag mode would only happen when cannot find id and will reach
         null on one side after RR or LL Rotation.
         */
        while(root->left != NULL || root->right != NULL)
            {
                if(id < root->getIdNo())
                    {
                        if(root->left == NULL)
                            break;
                    
                        if(id < root->left->getIdNo())
                            {
                                root = RightRotate(root); /* Only zig-zig mode need to rotate once,
                                                        because zig-zag mode is handled as zig
                                                        mode, which doesn't require rotate,
                                                        just linking it to R Tree */
                                if(!root->left)
                                    break;
                            }
                    
                        /* Link to Right Tree */
                        RightTreeMin->left = root;
                        RightTreeMin = RightTreeMin->left;
                        root = root->left;
                        RightTreeMin->left = NULL;
                    }
                else if(id > root->getIdNo())
                        {
                            if(root->right == NULL)
                                break;
                            if(id > root->right->getIdNo())
                                {
                                    root = LeftRotate(root);/* only zag-zag mode need to rotate once,
                                                        because zag-zig mode is handled as zag
                                                        mode, which doesn't require rotate,
                                                        just linking it to L Tree */
                                    if(root->right == NULL)
                                        break;
                                }
                            /* Link to Left Tree */
                            LeftTreeMax->right = root;
                            LeftTreeMax = LeftTreeMax->right;
                            root = root->right;
                            LeftTreeMax->right = NULL;
                        }
                    else
                        break;
                }
            /* assemble Left Tree, Middle Tree and Right tree together */
            LeftTreeMax->right = root->left;
            RightTreeMin->left = root->right;
            root->left = header->right;
            root->right = header->left;
            return root;
    }




SplayNode* SplayTree::findSplayNode(long val)
    {
        SplayNode* ptr = root;
        
        while ( ptr != NULL )
            {
                if  (ptr->getIdNo() == val)        // Found it!
                        {
                            return ptr;
                        }
            
                if (val < ptr->getIdNo() )
                    {
                        ptr = ptr->left;
                    }
                else
                    {
                        ptr = ptr->right;
                    }
            }
    
        return NULL;            // Tree is empty OR did not find it
    }






SplayNode* SplayTree::getParent(SplayNode* ptr)
    {
        if (ptr == NULL)
            return NULL;
        
        if (ptr == root)
            return NULL;
        
        SplayNode* cur = root;
        
        while ( cur != NULL )
            {
                if (cur->left == ptr || cur->right == ptr)        // Found it!
                    {
                        return cur;
                    }
                else
                    {
                        if (ptr->getIdNo() < cur->getIdNo())
                            cur = cur->left;
                        else
                            cur = cur->right;
                    }
            }
         return cur;            // Parent for ptr is NOT found
    }




SplayNode* SplayTree::getSuccessor(SplayNode* ptr)
{
    if (ptr->right == NULL)
        return ptr->left;
    else
        return(getMinimum(ptr->right));
}




SplayNode* SplayTree::getMinimum(SplayNode *ptr)
{
    while(ptr->left != NULL)
        ptr = ptr->left;

    return ptr;
}






SplayNode* SplayTree::InsertHelper(SplayNode* root, long id, string n, string sn, string m, float gpa)
    {
        SplayNode* newnode = new SplayNode(id, n, sn, m, gpa);
    
        if(root == NULL)
            return newnode;
    
        SplayNode* parent_temp = new SplayNode();
        SplayNode* temp = root;
    
        while(temp != NULL)
            {
                parent_temp = temp;
            
                if(id <= temp->getIdNo())
                    temp = temp->left;
                else
                    temp = temp->right;
            }
    
        if(id <= parent_temp->getIdNo())
            parent_temp->left = newnode;
        else
            parent_temp->right = newnode;
    
        root = Splay(root, id);
        return (root);
    }




void SplayTree::insert(long id, string n, string sn, string m, float gpa)
{
    root = InsertHelper(root, id, n, sn, m, gpa);

}





void SplayTree::remove(long str)
    {
        SplayNode* markedSplayNode = findSplayNode(str);
        deleteSplayNode(markedSplayNode);
    }





void SplayTree::deleteSplayNode(SplayNode* node2Zap)
    {

        if (node2Zap == NULL)    //If node2Zap is NULL then do nothing.
            return;
        else
            {
                SplayNode *successor, *successorChild, *parent;
            
                if (node2Zap->left == NULL || node2Zap->right == NULL)
                    successor = node2Zap;
                else                                        // node2Zap has 2 children
                    successor = getSuccessor(node2Zap);
            
 
                successorChild = successor->right;
                parent = getParent(successor);
            
            
                // Transfer data from successor to node2Zap
                node2Zap->setIdNo(successor->getIdNo());
                node2Zap->setName(successor->getName());
                node2Zap->setSurname(successor->getSurname());
                node2Zap->setMajor(successor->getMajor());
                node2Zap->setGpa(successor->getGpa());
            
            
                if ( parent == NULL)
                    root = successorChild;
                else if (successor == parent->left)
                        parent->left = successorChild;
                    else
                        parent->right = successorChild;
                
                if (successor != node2Zap){
                    node2Zap->setIdNo(successor->getIdNo());
                    node2Zap->setName(successor->getName());
                    node2Zap->setSurname(successor->getSurname());
                    node2Zap->setMajor(successor->getMajor());
                    node2Zap->setGpa(successor->getGpa());
                }
            
                delete successor;

            }
 
    }

string SplayTree::inorderHelper(SplayNode* ptr) {
    string nodeData = "";
    string leftChildId = "Null";
    string rightChildId = "Null";

    if (ptr != nullptr) {
        if (ptr->left != nullptr) {
            leftChildId = to_string(ptr->left->getIdNo());
        }
        if (ptr->right != nullptr) {
            rightChildId = to_string(ptr->right->getIdNo());
        }

        nodeData.append(inorderHelper(ptr->left));

        nodeData.append(to_string(ptr->getIdNo()) + "\t" + ptr->getName() + "\t" + ptr->getSurname() + "\t" + ptr->getMajor() + 
                    "\t" + to_string(ptr->getGpa()) + "\t\t" + "[" + leftChildId + ", " + rightChildId + "]" + "\n");

        nodeData.append(inorderHelper(ptr->right));
    }

    return nodeData;
}

string SplayTree::preorderHelper(SplayNode* ptr) {
    string nodeData = "";
    string leftChildId = "Null";
    string rightChildId = "Null";

    if (ptr != nullptr) {
        if (ptr->left != nullptr) {
            leftChildId = to_string(ptr->left->getIdNo());
        }
        if (ptr->right != nullptr) {
            rightChildId = to_string(ptr->right->getIdNo());
        }
        
        nodeData.append(to_string(ptr->getIdNo()) + "\t" + ptr->getName() + "\t" + ptr->getSurname() + "\t" + ptr->getMajor() + 
                    "\t" + to_string(ptr->getGpa()) + "\t\t" + "[" + leftChildId + ", " + rightChildId + "]" + "\n");

        nodeData.append(preorderHelper(ptr->left));
        nodeData.append(preorderHelper(ptr->right));
    }

    return nodeData;
}

string SplayTree::postorderHelper(SplayNode* ptr) {
    string nodeData = "";
    string leftChildId = "Null";
    string rightChildId = "Null";

    if (ptr != nullptr) {
        if (ptr->left != nullptr) {
            leftChildId = to_string(ptr->left->getIdNo());
        }
        if (ptr->right != nullptr) {
            rightChildId = to_string(ptr->right->getIdNo());
        }
        

        nodeData.append(postorderHelper(ptr->left));
        nodeData.append(postorderHelper(ptr->right));

        nodeData.append(to_string(ptr->getIdNo()) + "\t" + ptr->getName() + "\t" + ptr->getSurname() + "\t" + ptr->getMajor() + 
                    "\t" + to_string(ptr->getGpa()) + "\t\t" + "[" + leftChildId + ", " + rightChildId + "]" + "\n");
    }

    return nodeData;
}


#endif
