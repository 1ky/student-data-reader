#ifndef HEAP_H
#define HEAP_H

#include <string.h>

using namespace std;

class HeapNode {
private:
	long idNo;
	string name;
	string surname;
	string major;
	float gpa;

public:
	HeapNode* left;
	HeapNode* right;
	HeapNode* parent;

	HeapNode();
	HeapNode(long id, string n, string sn, string m, float g);

	void setIdNo(long id) { idNo = id; }
	void setName(string n) { name = n; }
	void setSurname(string sn) { surname = sn; }
	void setMajor(string m) { major = m; }
	void setGpa(float g) { gpa = g; }
	void setLeft(HeapNode* ptr) { left = ptr; }
	void setRight(HeapNode* ptr) { right = ptr; }
	void setParent(HeapNode* ptr) { parent = ptr; }

	long getIdNo() { return idNo; }
	string getName() { return name; }
	string getSurname() { return surname; }
	string getMajor() { return major; }
	float getGpa() { return gpa; }
	HeapNode* getLeft() { return left; }
	HeapNode* getRight() { return right; }
	
};

class Heap {
private:
	HeapNode* root;
	HeapNode* insertHelper(HeapNode* ptr, long id, string n, string sn, string m, float gpa);

	string inorderHelper(HeapNode* ptr);
	void heapify(HeapNode* ptr);
	int height(HeapNode* ptr);
	HeapNode* deleteNode(HeapNode*, long id);
public:
	Heap() { root = nullptr; }
	HeapNode* getRoot() { return root; }

	bool isEmpty();
	void purgeHeap() { root = nullptr; }
	void insert(long id, string n, string sn, string m, float gpa) { root = insertHelper(root, id, n, sn, m, gpa); }
	void remove(long id) { root = deleteNode(root, id);  }
	string inorder() { return inorderHelper(root); }
};

HeapNode::HeapNode() {
	string n = "";

	setIdNo(0);
	setName(n);
	setSurname(n);
	setMajor(n);
	setGpa(0);
	setLeft(nullptr);
	setRight(nullptr);
	setParent(nullptr);
}

HeapNode::HeapNode(long id, string n, string sn, string m, float g) {
	setIdNo(id);
	setName(n);
	setSurname(sn);
	setMajor(m);
	setGpa(g);
	setLeft(nullptr);
	setRight(nullptr);
	setParent(nullptr);
}

bool Heap::isEmpty() {
	if (root == nullptr)
		return true;
	return false;
}

HeapNode* Heap::insertHelper(HeapNode* ptr, long id, string n, string sn, string m, float gpa) {
	if (ptr == nullptr) {
		ptr = new HeapNode(id, n, sn, m, gpa);
		heapify(ptr);
		return ptr;
	}
	if (ptr->left == nullptr) {
		ptr->left = new HeapNode(id, n, sn, m, gpa);
		ptr->left->parent = ptr;
		heapify(ptr->left);

		return ptr;
	}
	if (ptr->right == nullptr) {
		ptr->right = new HeapNode(id, n, sn, m, gpa);
		ptr->right->parent = ptr;
		heapify(ptr->right);

		return ptr;
	}

	if (height(ptr->left) <= height(ptr->right)) {
		ptr->left = insertHelper(ptr->left, id, n, sn, m, gpa);
		heapify(ptr->left);
	} else {
		ptr->right = insertHelper(ptr->right, id, n, sn, m, gpa);
		heapify(ptr->right);
	}

	return ptr;
}

int Heap::height(HeapNode* ptr) {
	int leftHeight, rightHeight;
        
        leftHeight = rightHeight = 0;
        
        if( ptr == NULL)
            return(0);
        
        if(ptr->left == NULL)
            leftHeight = 0;
        else
            leftHeight = 1 + height(ptr->left);
        
        if(ptr->right == NULL)
            rightHeight = 0;
        else
            rightHeight = 1 + height(ptr->right);
        
        
        if(leftHeight > rightHeight)
            return(rightHeight);
        else
            return(leftHeight);
}

void Heap::heapify(HeapNode* ptr) {
	HeapNode* ancestor = ptr->parent;
    long temp;

        
    if (ptr->parent == NULL)    // ptr is the root - has no parent
        return;

    while (ptr->getIdNo() > ancestor->getIdNo()) {
        temp = ancestor->getIdNo();
        ancestor->setIdNo(ptr->getIdNo());
        ptr->setIdNo(temp);

        ptr = ancestor;
        if (ptr == root)
            return;
        else
            ancestor = ptr->parent;
    }
        
    return;
}

string Heap::inorderHelper(HeapNode* ptr) {
	string nodeData = "";
	string leftChildId = "Null";
	string rightChildId = "Null";

	if (ptr != nullptr)	{
		if (ptr->left != nullptr) {
			leftChildId = to_string(ptr->left->getIdNo());
		}
		if (ptr->right != nullptr) {
			rightChildId = to_string(ptr->right->getIdNo());
		}

		nodeData.append(inorderHelper(ptr->left));

		nodeData.append(to_string(ptr->getIdNo()) + "\t" + ptr->getName() + "\t" + ptr->getSurname() + "\t" + ptr->getMajor() + 
					"\t" + to_string(ptr->getGpa()) + "\t\t" + "[" + leftChildId + ", " + rightChildId + "]" + "\n");

		nodeData.append(inorderHelper(ptr->right));
	}

	return nodeData;
}

HeapNode* Heap::deleteNode(HeapNode* ptr, long id) {
	HeapNode* successor;

	if (ptr == nullptr)
		return nullptr;
	else {
		if (id == ptr->getIdNo()) {
			if (ptr->right != nullptr) {
				successor = ptr->right;
				while (successor->left != nullptr)
					successor = successor->left;

				ptr->setIdNo(successor->getIdNo());
				ptr->setName(successor->getName());
				ptr->setSurname(successor->getSurname());
				ptr->setMajor(successor->getMajor());
				ptr->setGpa(successor->getGpa());

				ptr->right = deleteNode(ptr->right, ptr->getIdNo());
			}
			else
				return ptr->left;
		}
		ptr->left = deleteNode(ptr->left, id);
		ptr->right = deleteNode(ptr->right, id);
	}
	return ptr;
}

#endif