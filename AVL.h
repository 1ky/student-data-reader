#ifndef AVL_H
#define AVL_H

#include <string.h>

using namespace std;

class AVLNode {
private:
	long idNo;
	string name;
	string surname;
	string major;
	float gpa;

	int height;

public:
	AVLNode* left;
	AVLNode* right;

	AVLNode();
	AVLNode(long id, string n, string sn, string m, float g);

	void setIdNo(long id) { idNo = id; }
	void setName(string n) { name = n; }
	void setSurname(string sn) { surname = sn; }
	void setMajor(string m) { major = m; }
	void setGpa(float g) { gpa = g; }
	void setLeft(AVLNode* ptr) { left = ptr; }
	void setRight(AVLNode* ptr) { right = ptr; }
	void setHeight(int h) { height = h; }

	long getIdNo() { return idNo; }
	string getName() { return name; }
	string getSurname() { return surname; }
	string getMajor() { return major; }
	float getGpa() { return gpa; }
	AVLNode* getLeft() { return left; }
	AVLNode* getRight() { return right; }
	int getHeight() { return height; }
};

class AVL {
private:
	AVLNode* root;

	AVLNode* insertHelper(AVLNode* ptr, long id, string n, string sn, string m, float gpa);
	AVLNode* deleteNode(AVLNode*, long id);

	string inorderHelper(AVLNode* ptr);
	string preorderHelper(AVLNode* ptr);
	string postorderHelper(AVLNode* ptr);

	AVLNode* rotateRight(AVLNode* ptr);
	AVLNode* rotateLeft(AVLNode* ptr);
	AVLNode* rotateRightLeft(AVLNode* ptr);
	AVLNode* rotateLeftRight(AVLNode* ptr);
	AVLNode* rotateDoubleRight(AVLNode* ptr);
	AVLNode* rotateDoubleLeft(AVLNode* ptr);

	int calcHeight(AVLNode* ptr);
	int calcBalance(AVLNode* ptr);

public:
	AVL() { root = nullptr; }

	bool isEmpty();
	void purgeAVL() { root = nullptr; }

	void insert(long id, string n, string sn, string m, float gpa) { root = insertHelper(root, id, n, sn, m, gpa); }
	void remove(long id) { root = deleteNode(root, id); }

	string inorder() { return inorderHelper(root); }
	string preorder() { return preorderHelper(root); }
	string postorder() { return postorderHelper(root); }
};

AVLNode::AVLNode() {
	string n = "";

	setIdNo(0);
	setName(n);
	setSurname(n);
	setMajor(n);
	setGpa(0);
	setLeft(nullptr);
	setRight(nullptr);
	setHeight(0);
}

AVLNode::AVLNode(long id, string n, string sn, string m, float g) {
	setIdNo(id);
	setName(n);
	setSurname(sn);
	setMajor(m);
	setGpa(g);
	setLeft(nullptr);
	setRight(nullptr);
	setHeight(0);
}

bool AVL::isEmpty() {
	if (root == nullptr)
		return true;
	return false;
}

AVLNode* AVL::insertHelper(AVLNode* ptr, long id, string n, string sn, string m, float gpa) {
	if (ptr == nullptr)
		ptr = new AVLNode(id, n, sn, m, gpa);
	else {
		if (id >= ptr->getIdNo()) {
			ptr->right = insertHelper(ptr->right, id, n, sn, m, gpa);
			if (calcBalance(ptr) == -2) {
				if (id > ptr->right->getIdNo())
					ptr = rotateDoubleRight(ptr);
				else
					ptr = rotateRightLeft(ptr);
			}
		}
		else {
			if (id < ptr->getIdNo()) {
				ptr->left = insertHelper(ptr->left, id, n, sn, m, gpa);
				if (calcBalance(ptr) == 2) {
					if (id < ptr->left->getIdNo())
						ptr = rotateDoubleLeft(ptr);
					else
						ptr = rotateLeftRight(ptr);
				}
			}
		}
	}
	ptr->setHeight(calcHeight(ptr));
	return ptr;
}

string AVL::inorderHelper(AVLNode* ptr) {
	string nodeData = "";
	string leftChildId = "Null";
	string rightChildId = "Null";

	if (ptr != nullptr)	{
		if (ptr->left != nullptr) {
			leftChildId = to_string(ptr->left->getIdNo());
		}
		if (ptr->right != nullptr) {
			rightChildId = to_string(ptr->right->getIdNo());
		}

		nodeData.append(inorderHelper(ptr->left));

		nodeData.append(to_string(ptr->getIdNo()) + "\t" + ptr->getName() + "\t" + ptr->getSurname() + "\t" + ptr->getMajor() + 
					"\t" + to_string(ptr->getGpa()) + "\t\t" + "[" + leftChildId + ", " + rightChildId + "]" + " ["+ 
					to_string(calcBalance(ptr)) + "]" + "\n");

		nodeData.append(inorderHelper(ptr->right));
	}

	return nodeData;
}

string AVL::preorderHelper(AVLNode* ptr) {
	string nodeData = "";
	string leftChildId = "Null";
	string rightChildId = "Null";

	if (ptr != nullptr)	{
		if (ptr->left != nullptr) {
			leftChildId = to_string(ptr->left->getIdNo());
		}
		if (ptr->right != nullptr) {
			rightChildId = to_string(ptr->right->getIdNo());
		}
		
		nodeData.append(to_string(ptr->getIdNo()) + "\t" + ptr->getName() + "\t" + ptr->getSurname() + "\t" + ptr->getMajor() + 
					"\t" + to_string(ptr->getGpa()) + "\t\t" + "[" + leftChildId + ", " + rightChildId + "]" + " ["+ 
					to_string(calcBalance(ptr)) + "]" + "\n");

		nodeData.append(preorderHelper(ptr->left));
		nodeData.append(preorderHelper(ptr->right));
	}

	return nodeData;
}

string AVL::postorderHelper(AVLNode* ptr) {
	string nodeData = "";
	string leftChildId = "Null";
	string rightChildId = "Null";

	if (ptr != nullptr)	{
		if (ptr->left != nullptr) {
			leftChildId = to_string(ptr->left->getIdNo());
		}
		if (ptr->right != nullptr) {
			rightChildId = to_string(ptr->right->getIdNo());
		}
		

		nodeData.append(postorderHelper(ptr->left));
		nodeData.append(postorderHelper(ptr->right));

		nodeData.append(to_string(ptr->getIdNo()) + "\t" + ptr->getName() + "\t" + ptr->getSurname() + "\t" + ptr->getMajor() + 
					"\t" + to_string(ptr->getGpa()) + "\t\t" + "[" + leftChildId + ", " + rightChildId + "]" + " ["+ 
					to_string(calcBalance(ptr)) + "]" + "\n");
	}

	return nodeData;
}

AVLNode* AVL::rotateRight(AVLNode* ptr) {
	AVLNode* newParent;

	newParent = ptr->left;
	ptr->left = newParent->right;
	newParent->right = ptr;
	ptr->setHeight(calcHeight(ptr));
	newParent->setHeight(calcHeight(newParent));

	return newParent;
}

AVLNode* AVL::rotateLeft(AVLNode* ptr) {
	AVLNode* newParent;

	newParent = ptr->right;
	ptr->right = newParent->left;
	newParent->left = ptr;
	ptr->setHeight(calcHeight(ptr));
	newParent->setHeight(calcHeight(newParent));

	return newParent;
}

AVLNode* AVL::rotateRightLeft(AVLNode* ptr) {
	ptr->right = rotateRight(ptr->right);
	ptr = rotateLeft(ptr);
	return ptr;
}

AVLNode* AVL::rotateLeftRight(AVLNode* ptr) {
	ptr->left = rotateLeft(ptr->left);
	ptr = rotateRight(ptr);
	return ptr;
}

AVLNode* AVL::rotateDoubleRight(AVLNode* ptr) {
	ptr = rotateLeft(ptr);
	return ptr;
}

AVLNode* AVL::rotateDoubleLeft(AVLNode* ptr) {
	ptr = rotateRight(ptr);
	return ptr;
}

int AVL::calcHeight(AVLNode* ptr) {
	int leftHeight, rightHeight;

	if (ptr->left == nullptr)
		leftHeight = 0;
	else
		leftHeight = 1 + ptr->left->getHeight();

	if (ptr->right == nullptr)
		rightHeight = 0;
	else
		rightHeight = 1 + ptr->right->getHeight();

	if (leftHeight > rightHeight)
		return leftHeight;
	return rightHeight;
}

int AVL::calcBalance(AVLNode* ptr) {
	int leftHeight, rightHeight;

	if (ptr->left == nullptr)
		leftHeight = 0;
	else
		leftHeight = 1 + ptr->left->getHeight();

	if (ptr->right == nullptr)
		rightHeight = 0;
	else
		rightHeight = 1 + ptr->right->getHeight();

	return (leftHeight - rightHeight);
}

AVLNode*  AVL::deleteNode(AVLNode* ptr, long val)
{
    AVLNode* successor;
    
    if ( ptr == NULL)           // Node location is empty
        {
            return NULL;
        }
    else
        if( val > ptr->getIdNo() )                // Search in Right sub-tree
            {
                ptr->right = deleteNode(ptr->right, val);
                if ( calcBalance(ptr) == 2)
                    {
                        if(calcBalance(ptr->left) >= 0)
                            ptr = rotateDoubleLeft(ptr);
                        else
                            ptr = rotateLeftRight(ptr);
                    }
            }
        else
            if( val < ptr->getIdNo() )          // Search the Left sub-tree
                {
                    ptr->left = deleteNode(ptr->left, val);
                    if( calcBalance(ptr) == -2 )                   //Rebalance during windup
                        {
                            if( calcBalance(ptr->right) <= 0)
                                ptr = rotateDoubleRight(ptr);
                            else
                                ptr = rotateRightLeft(ptr);
                        }
                }
            else
                {
                    //Node to to be deleted is found
                    if ( ptr->right != NULL)
                        {  //delete its inorder succesor
                            successor = ptr->right;
                            while ( successor->left != NULL)
                                successor = successor->left;
                            
                            ptr->setIdNo( successor->getIdNo() );
                            ptr->right = deleteNode( ptr->right, ptr->getIdNo() );
                            if(calcBalance(ptr) == 2) // Rebalance during windup
                                {
                                    if(calcBalance(ptr->left) >= 0)
                                        ptr = rotateDoubleLeft(ptr);
                                    else
                                        ptr = rotateLeftRight(ptr);
                                }
                    }
                else
                    return(ptr->left);
                }
    ptr->setHeight(calcHeight(ptr));
    return(ptr);
}

#endif