#ifndef SET_H
#define SET_H

#include <iostream>
#include "Node.h"

using namespace std;

class Set {
private:
	Node* frontPtr;
public:
	Set(void);

	void addTail(long id, string n, string sn, string m, float g);
	void addHead(long id, string n, string sn, string m, float g);
	void delNode(long id);
	void purgeSet();

	Node* getNode(long id, string n, string sn, string m, float g);
	string showNodes(void);
	string showCompNodes(void);
	Set copySet(void);

	bool isEmpty();
	string showIntersection(Set S);
	string showUnion(Set S);
};

Set::Set() {
	frontPtr = new Node();
}

void Set::addTail(long id, string n, string sn, string m, float g) {
	if (getNode(id, n, sn, m, g) == nullptr) {
		Node* iPtr = frontPtr;
		while (iPtr->getNext() != nullptr) {
			iPtr = iPtr->getNext();
		}

		Node* newPtr = new Node(id, n, sn, m, g);
		newPtr->setNext(iPtr->getNext());
		iPtr->setNext(newPtr);
	}
}

void Set::addHead(long id, string n, string sn, string m, float g) {
	//std::cout << "hh";
	if (getNode(id, n, sn, m, g) == nullptr) {
		Node* newPtr = new Node(id, n, sn, m, g);
		newPtr->setNext(frontPtr->getNext());
		frontPtr->setNext(newPtr);
	}
}

void Set::delNode(long id) {
	Node* iPtr = frontPtr;
	while (iPtr != nullptr && iPtr->getNext() != nullptr) {
		if (iPtr->getNext()->getIdNo() == id) {
			Node* tempPtr = iPtr->getNext();
			iPtr->setNext(tempPtr->getNext());
			delete tempPtr;
		}
		iPtr = iPtr->getNext();
	}
}

Node* Set::getNode(long id, string n, string sn, string m, float g) {
	if (!isEmpty()){
		Node* iPtr = frontPtr;
		while (iPtr != nullptr) {
			if (iPtr->getIdNo() == id) {
				if (iPtr->getName() == n)
					if (iPtr->getSurname() == sn)
						if (iPtr->getMajor() == m)
							if (iPtr->getGpa() == g)
								return iPtr;
			}
			iPtr = iPtr->getNext();
		}
	}
	return nullptr;
}

string Set::showNodes(void) {
	Node* iPtr = frontPtr->getNext();
	string nodeData = "";

	while (iPtr != nullptr) {
		nodeData.append(to_string(iPtr->getIdNo()) + "\t" + iPtr->getName() + "\t" + iPtr->getSurname() + "\t" + iPtr->getMajor() + 
					"\t" + to_string(iPtr->getGpa()) + "\n");

		iPtr = iPtr->getNext();
	}

	return nodeData;
}

string Set::showCompNodes(void) {
	Node* iPtr = frontPtr->getNext();
	string nodeData = "";

	while (iPtr != nullptr) {
		if (iPtr->getMajor() == "COMP") {
			nodeData.append(to_string(iPtr->getIdNo()) + "\t" + iPtr->getName() + "\t" + iPtr->getSurname() + "\t" + iPtr->getMajor() + 
						"\t" + to_string(iPtr->getGpa()) + "\n");
		}

		iPtr = iPtr->getNext();
	}

	return nodeData;
}

bool Set::isEmpty(void) {
	if (frontPtr->getNext() == nullptr)
		return true;

	return false;
}

void Set::purgeSet(void) {
	frontPtr = new Node();
}

string Set::showIntersection(Set S) {
	string nodeData = "";

	nodeData.append(showCompNodes());
	nodeData.append(S.showCompNodes());

	return nodeData;
}

string Set::showUnion(Set S) {
	Set T = S.copySet();
	string nodeData = "";

	Node* iPtr = frontPtr->getNext();
	while (iPtr != nullptr) {
		T.addTail(iPtr->getIdNo(), iPtr->getName(), iPtr->getSurname(), iPtr->getMajor(), iPtr->getGpa());

		iPtr = iPtr->getNext();
	}

	nodeData.append(T.showNodes());
	return nodeData;
}

Set Set::copySet() {
	Set S;

	Node* iPtr = frontPtr->getNext();
	while (iPtr != nullptr) {
		S.addTail(iPtr->getIdNo(), iPtr->getName(), iPtr->getSurname(), iPtr->getMajor(), iPtr->getGpa());

		iPtr = iPtr->getNext();
	}

	return S;
}

#endif
