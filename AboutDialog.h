#ifndef ABOUTDIALOG_H
#define ABOUTDIALOG_H

#include <wx/version.h>


   
  
//Define a dialog box class with 5 data fields (Attributes) - inheriting from wxDialog, of course 
class AboutDialog: public wxDialog
  {
	 public:
		AboutDialog(const wxString& title, const wxPoint& pos, const wxSize& size);

  };

  
  
AboutDialog::AboutDialog ( const wxString& title, const wxPoint& pos, 
		   const wxSize& size): wxDialog((wxDialog *)NULL, -1, title, pos, size)
  {
  		wxString versionDetails;
  		versionDetails.Printf(wxT("Project Version: \t%s"), wxVERSION_STRING);

		//Set up the panel
  		wxPanel    *panel = new wxPanel(this, -1);
 
		//Define and position the StaticTexts - ie the labels
		wxStaticText *idNoLabel = new wxStaticText(panel, wxID_ANY,
						  wxT("Students Name: \tKyle Shepherd"), wxPoint(15, 25) );
  
		wxStaticText *nameLabel 	= new wxStaticText(panel, wxID_ANY,
						  wxT("Students ID: \t\t417000786"), wxPoint(15, 60) );
  
		wxStaticText *surnameLabel 	= new wxStaticText(panel, wxID_ANY,
						  versionDetails, wxPoint(15, 95) );

		wxStaticText *gpaLabel = new wxStaticText(panel, wxID_ANY,
						wxT("I'm very happy to have finally completed this Project.\nIt took literal weeks to finish, guess I should have started earlier.\nI better be getting pretty close to full marks on this."), wxPoint(15, 165) );
  
 		wxStaticText *majorLabel	= new wxStaticText(panel, wxID_ANY,
						  wxT("Students Email: \tkyle.shepherd@mycavehill.uwi.edu"), wxPoint(15, 130) );
		
        
		// Define and position the Response buttons (OK and CANCEL)
		
		// The OK button
		wxButton* ok = new wxButton(panel, wxID_OK, wxT("&OK"), 
					       wxPoint(200, 230), wxDefaultSize, 0);
		
  		// Centre the dialog on the parent's window
		Centre();

        //  Maximize(); // Maximize the window
		
  }

#endif