#ifndef Node_h
#define Node_h

#include <string.h>

using namespace std;

class Node {
private:
	long idNo;
	string name;
	string surname;
	string major;
	float gpa;

	Node* Next;

public:
	Node();
	Node(long id, string n, string sn, string m, float g);

	void setIdNo(long id) { idNo = id; }
	void setName(string n) { name = n; }
	void setSurname(string sn) { surname = sn; }
	void setMajor(string m) { major = m; }
	void setGpa(float g) { gpa = g; }
	void setNext(Node* ptr) { Next = ptr; }

	long getIdNo() { return idNo; }
	string getName() { return name; }
	string getSurname() { return surname; }
	string getMajor() { return major; }
	float getGpa() { return gpa; }
	Node* getNext() { return Next; }
};

Node::Node() {
	string n = "";

	setIdNo(0);
	setName(n);
	setSurname(n);
	setMajor(n);
	setGpa(0);
	setNext(nullptr);
}

Node::Node(long id, string n, string sn, string m, float g) {
	setIdNo(id);
	setName(n);
	setSurname(sn);
	setMajor(m);
	setGpa(g);
	setNext(nullptr);
}

#endif
